
/* @@@ Sensor :
 * firstname-input
 * secondname-input
 * mail-input
 * mobile-input
 * country-input
 * birthday-input
 * gender-input
 * password-input
 **/

$(document).ready(function()
{
   var signup_sensor_arr = ['#firstname-input', '#secondname-input', '#mail-input', '#mobile-input', 
   '#country-input', '#birthday-input', '#gender-input', '#pw_1-input', '#pw_2-input', '#mail_pw_lost-input'];
   
   var signup_text_arr = ['#firstname-text', '#secondname-text', '#mail-text', '#mobile-text', 
   '#country-text', '#birthday-text', '#gender-text', '#pw_1-text', '#pw_2-text', '#mail_pw_lost-text'];
   
   //hide all elements
   for (i = 0; i < signup_text_arr.length; i++) 
   {
      $(signup_text_arr[i]).fadeOut();
      $(signup_text_arr[i]).fadeOut(100);
      change_placeholder_color(signup_sensor_arr[i], 'grey', 'bold', '0.95rem')
   }
   
   //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
   
   var i;
   
   //manage the placeholder and info label for each textbox
   for (i = 0; i < signup_sensor_arr.length; i++) 
   {
      $(signup_sensor_arr[i]).click
      (
         function()
         {
            //re-copy of all the input (which in our case are buttons)
            var signup_sensor_arr = ['#firstname-input', '#secondname-input', '#mail-input', '#mobile-input', 
            '#country-input', '#birthday-input', '#gender-input', '#pw_1-input', '#pw_2-input', '#mail_pw_lost-input'];
            
            var signup_text_arr = ['#firstname-text', '#secondname-text', '#mail-text', '#mobile-text', 
            '#country-text', '#birthday-text', '#gender-text', '#pw_1-text', '#pw_2-text', '#mail_pw_lost-text'];
            
            var signup_placeholder_arr = ['First name', 'Second name', 'Email', 'Mobile number', 
            'Country', 'Birthday DD/MM/YYYY', 'Gender', 'Password', 'Password again', 'Email'];
            
            //remove the button from the array the on click id elem (current one)
            var elem_sensor = "#" + event.srcElement.id;
            var pos = signup_sensor_arr.indexOf(elem_sensor);
            
            var elem_text = signup_text_arr[pos];
            
            var signup_sensor_arr_tmp = signup_sensor_arr.splice(pos, 1);
            var signup_placeholder_arr_tmp = signup_placeholder_arr.splice(pos, 1);
            var signup_text_arr_tmp = signup_text_arr.splice(pos, 1);
            
            //generate hide animation for all the other texts
            for (i = 0; i < signup_text_arr.length; i++) 
            {
               $(signup_text_arr[i]).slideUp();
               $(signup_sensor_arr[i]).attr('placeholder', signup_placeholder_arr[i]);
               change_placeholder_color(signup_sensor_arr[i], 'grey', 'bold', '0.95rem', 'none');
            }
            
            //pop out the text of the current textbox
            $(elem_text).slideDown("slow");
            $(elem_sensor).attr('placeholder', '');
         }
      );
   }
   
   //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
   
   //change placeholder style easily
   function change_placeholder_color(target_class, color_choice, font_weight, font_size, text_transform) 
   {
      $("body").append(
         "<style>" + 
            target_class + "::placeholder{" +
               "color:" +  color_choice + ";" +
               "font-size: " + font_size + ";" +
               "text-transform: " + text_transform + ";" +
               "font-family: 'Spartan', sans-serif;" +
               "font-weight: " + font_weight + ";" +
            "}" +
         "</style>")
   }
   
   //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
   
   //release the fail span when the checkbox isChecked
   var signup_sensor_check_arr = ['#banking_cash_signup', '#judgement_arbitration_signup', 
         '#policy_terms_signup'];
   
   for (i = 0; i < signup_sensor_check_arr.length; i++) 
   {
      $(signup_sensor_check_arr[i]).click
      (
         function() 
         {
            var signup_sensor_check_arr = ['#banking_cash_signup', '#judgement_arbitration_signup', 
            '#policy_terms_signup'];
   
            var signup_sensor_checkspan_arr = ['#banking_cash_fail', '#judgement_arbitration_fail', 
            '#policy_terms_fail'];
            
            var elem = "#" + event.srcElement.id;
            var pos = signup_sensor_check_arr.indexOf(elem);
            
            $(signup_sensor_checkspan_arr[pos]).text("");
         }
      );
   }
   
   //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
   
   //handle the login page
   $("#login_btn").click(
      function()
      {
         var flag_errors = 0;
         
         //check general emptiness 
         if($('#mail-input').val().length == 0)
         {
            flag_errors = 1;
            change_placeholder_color('#mail-input', 'red', 'bold', '0.95rem', 'none');
            $('#mail-input').attr('placeholder', 'must be filled');
         }
         
         if($('#pw_1-input').val().length == 0)
         {
            flag_errors = 1;
            change_placeholder_color('#pw_1-input', 'red', 'bold', '0.95rem', 'none');
            $('#pw_1-input').attr('placeholder', 'must be filled');
         }
         
         //check the mail @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         if (isNaN($('#mail-input').val()))
         {
            var char_check_1 = $('#mail-input').val().indexOf("@");

            if (char_check_1 >= 0)
            {
               var char_check_2 = $('#mail-input').val().indexOf(".");
               
               // missing the dns
               if (char_check_2 < 0)
               {
                  flag_errors = 1;
                  change_placeholder_color('#mail-input', 'red', 'bold', '0.95rem', 'none');
                  $('#mail-input').attr('placeholder', 'you\'re missing the subdomain');
                  $('#mail-input').val('');
               }
            }
            // missing the @ char
            else 
            {
               flag_errors = 1;
               change_placeholder_color('#mail-input', 'red', 'bold', '0.95rem', 'none');
               $('#mail-input').attr('placeholder', 'you\'re missing the @');
               $('#mail-input').val('');
            }
         }

         if(flag_errors == 0) 
         {
            $('#login_form').submit();
         }
         else 
         {
            return false;
         }
      }
   );
   
   //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
   
   //handle the mail reset password
   $("#pwlost_btn").click(
      function()
      {
         var flag_errors = 0;
         
         //check general emptiness 
         if($('#mail_pw_lost-input').val().length == 0)
         {
            flag_errors = 1;
            change_placeholder_color('#mail_pw_lost-input', 'red', 'bold', '0.95rem', 'none');
            $('#mail_pw_lost-input').attr('placeholder', 'must be filled');
         }
         
         //check the mail @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         if (isNaN($('#mail_pw_lost-input').val()))
         {
            var char_check_1 = $('#mail_pw_lost-input').val().indexOf("@");

            if (char_check_1 >= 0)
            {
               var char_check_2 = $('#mail_pw_lost-input').val().indexOf(".");
               
               // missing the dns
               if (char_check_2 < 0)
               {
                  flag_errors = 1;
                  change_placeholder_color('#mail_pw_lost-input', 'red', 'bold', '0.95rem', 'none');
                  $('#mail_pw_lost-input').attr('placeholder', 'you\'re missing the subdomain');
                  $('#mail_pw_lost-input').val('');
               }
            }
            // missing the @ char
            else 
            {
               flag_errors = 1;
               change_placeholder_color('#mail_pw_lost-input', 'red', 'bold', '0.95rem', 'none');
               $('#mail_pw_lost-input').attr('placeholder', 'you\'re missing the @');
               $('#mail_pw_lost-input').val('');
            }
         }

         if(flag_errors == 0) 
         {
            $('#lostpw_form').submit();
         }
         else 
         {
            return false;
         }
      }
   );
   
   //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
   
   //handle the password reset
   $("#resetpw_btn").click(
      function()
      {
         var flag_errors = 0;
         
         //check password size
         if ($('#pw_1-input').val().length < 8)
         {
            flag_errors = 1;
            change_placeholder_color('#pw_1-input', 'red', 'bold', '0.95rem', 'none');
            $('#pw_1-input').attr('placeholder', 'minimum size 8 chars');
            $('#pw_1-input').val('');
         }
         
         //password doesn't match
         if ($('#pw_1-input').val() != ($('#pw_2-input').val()))
         {
            flag_errors = 1;
            $('#password_fail').text("Password doesn't match!");
         }

         if(flag_errors == 0) 
         {
            $('#password_reset').submit();
         }
         else 
         {
            return false;
         }
      }
   );
   
   //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
   
   //handle the signup
   $("#signup_btn").click(
      function()
      {
         
         var flag_errors = 0;
         
         //check the emptiness of textfields @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         var signup_sensor_arr = ['#firstname-input', '#secondname-input', '#mail-input', '#mobile-input', 
         '#country-input', '#birthday-input', '#gender-input'];
         
         for (i = 0; i < signup_sensor_arr.length; i++) 
         {
            //check general emptiness 
            if($(signup_sensor_arr[i]).val().length == 0)
            {
               flag_errors = 1;
               change_placeholder_color(signup_sensor_arr[i], 'red', 'bold', '0.95rem', 'none');
               $(signup_sensor_arr[i]).attr('placeholder', 'must be filled');
            }
         }
         
         //check the mobile number @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         if (isNaN($('#mobile-input').val()))
         {
            flag_errors = 1;
            change_placeholder_color('#mobile-input', 'red', 'bold', '0.95rem', 'none');
            $('#mobile-input').attr('placeholder', 'only digits');
            $('#mobile-input').val('');
         }
         
         //check the birthday date @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         if (isNaN($('#birthday-input').val()))
         {
            //check the length
            if ($('#birthday-input').val().length != 10)
            {
               flag_errors = 1;
               change_placeholder_color('#birthday-input', 'red', 'bold', '0.95rem', 'none');
               $('#birthday-input').attr('placeholder', 'wrong size must be DD/MM/YYYY');
               $('#birthday-input').val('');
            }
            else 
            {
               //check if digits are correct
               if (isNaN($('#birthday-input').val()[0]) || 
                   isNaN($('#birthday-input').val()[1]) ||
                   isNaN($('#birthday-input').val()[3]) ||
                   isNaN($('#birthday-input').val()[4]) ||
                   isNaN($('#birthday-input').val()[6]) ||
                   isNaN($('#birthday-input').val()[7]) ||
                   isNaN($('#birthday-input').val()[8]) ||
                   isNaN($('#birthday-input').val()[9]))
               {
                  flag_errors = 1;
                  change_placeholder_color('#birthday-input', 'red', 'bold', '0.95rem', 'none');
                  $('#birthday-input').attr('placeholder', 'X must be digits : XX/XX/XXXX');
                  $('#birthday-input').val('');
               }
               else 
               {
                  //check if the slash are correct
                  if (($('#birthday-input').val()[2] != '/') ||
                      ($('#birthday-input').val()[5] != '/'))
                  {
                     flag_errors = 1;
                     change_placeholder_color('#birthday-input', 'red', 'bold', '0.95rem', 'none');
                     $('#birthday-input').attr('placeholder', 'X must be \'/\' : DDXMMXYYYY');
                     $('#birthday-input').val('');
                  }
               }
            }
         }
         //can't be a pure digit
         else 
         {
            flag_errors = 1;
            change_placeholder_color('#birthday-input', 'red', 'bold', '0.95rem', 'none');
            $('#birthday-input').attr('placeholder', 'wrong format DD/MM/YYYY');
            $('#birthday-input').val('');
         }
         
         //check the mail @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         if (isNaN($('#mail-input').val()))
         {
            var char_check_1 = $('#mail-input').val().indexOf("@");

            if (char_check_1 >= 0)
            {
               var char_check_2 = $('#mail-input').val().indexOf(".");
               
               // missing the dns
               if (char_check_2 < 0)
               {
                  flag_errors = 1;
                  change_placeholder_color('#mail-input', 'red', 'bold', '0.95rem', 'none');
                  $('#mail-input').attr('placeholder', 'you\'re missing the subdomain');
                  $('#mail-input').val('');
               }
            }
            // missing the @ char
            else 
            {
               flag_errors = 1;
               change_placeholder_color('#mail-input', 'red', 'bold', '0.95rem', 'none');
               $('#mail-input').attr('placeholder', 'you\'re missing the @');
               $('#mail-input').val('');
            }
         }
         //can't be a digit
         else if ($('#mobile-input').val().length != 0)
         {
            flag_errors = 1;
            change_placeholder_color('#mail-input', 'red', 'bold', '0.95rem', 'none');
            $('#mail-input').attr('placeholder', 'can\'t be a digit');
            $('#mail-input').val('');
         }
         
         //check the checkbox policy completion @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         var signup_sensor_check_arr = ['#banking_cash_signup', '#judgement_arbitration_signup', 
         '#policy_terms_signup'];
         
         var signup_sensor_checkspan_arr = ['#banking_cash_fail', '#judgement_arbitration_fail', 
         '#policy_terms_fail'];
         
         for (i = 0; i < signup_sensor_check_arr.length; i++) 
         {
            if(($(signup_sensor_check_arr[i]).is(":checked")) == 0)
            {
               flag_errors = 1;
               $(signup_sensor_checkspan_arr[i]).text("MUST BE CHECKED");
            }
         }
         
         //if everything correct move to the next page @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
         if(flag_errors == 0) 
         {
            $('#signup_form').submit();
         }
         else 
         {
            return false;
         }
      }
   );
});


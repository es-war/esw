
$(document).ready(function()
{
   $('#loginform').submit(function(e) {
      e.preventDefault();
      $.ajax({
         type: "POST",
         url: 'login.php',
         data: $(this).serialize(),
            success: function(response)
            {
               var jsonData = JSON.parse(response);

               // user is logged in successfully in the back-end
               // let's redirect
               if (jsonData.success == "1")
               {
                  alert(jsonData.name);
               }
               else
               {
                  alert('Invalid Credentials!');
               }
            }
      });
   });
});


<?php

   $GLOBALS['dns'] = "http://localhost/xampp"; //the path to www
   $GLOBALS['access_log'] = "C:\\xampp\\apache\\logs\\access.log"; //access log path
   $GLOBALS['session_lifetime'] = 3600; //time for user sessions
   
   session_start();
   require_once('esw_system/esw_core/session_handler.php');
   session_handler::manage();
   
?>

<!DOCTYPE html>

<html lang="en">

   <head>

      <!-- meta data -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">

      <title>ESW</title>

      <!-- Custom fonts for this template-->
      <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
      <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
      <link rel="preconnect" href="https://fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css2?family=Bungee&display=swap" rel="stylesheet">

      <!-- Custom styles for this template-->
      <link href="css/sb-admin-2.css" rel="stylesheet">

   </head>

<?php 
   
   $usr_act = (isset($_GET['action']) ? $_GET['action'] : 100);

   echo "<body id='page-top' class='bg-signup'>
            <div class='wrapper'>";

   switch($usr_act) {
      case 1: 
         include 'esw_module/esw_login/login_info.php';
      break;
      case 2:
         include 'esw_module/esw_signup/signup_info.php';
      break;
      case 3:
         include 'esw_module/esw_signup/signup_ack.php';
      break;
      case 4:
         include 'esw_system/esw_core/cron_task/cron_mailing.php';
      break;
      case 5:
         include 'esw_module/esw_login/login_ack.php';
      break;
      case 6:
         include 'esw_module/esw_login/login_password.php';
      break;
      case 7:
         include 'esw_module/esw_profil/profil_main.php';
      break;
      case 8:
         session_handler::echo();
      break;
      case 9:
         session_handler::end();
         echo "<center>session killed</center>";
      break;
      case 10:
         echo "<center>Are you authorized : ".session_handler::is_auth()."</center><br>";
         echo "<center>-1 = currently not logged</center><br>";
         echo "<center>0 = banned</center><br>";
         echo "<center>1 = normal user</center><br>";
         echo "<center>2 = rank 2 user</center><br>";
      break;
      case 11:
         include 'esw_system/esw_core/cron_task/cron_tracking.php';
      break;
      case 12:
         echo '<form id="loginform" method="post">
                  <div>
                     Username:
                     <input type="text" name="username" id="username" />
                     Password:
                     <input type="password" name="password" id="password" />    
                     <input type="submit" name="loginBtn" id="loginBtn" value="Login" />
                  </div>
               </form>';
      break;
   }
   
   echo "   </div>";
   
?>

      <!-- Bootstrap core JavaScript-->
      <script src='vendor/jquery/jquery.min.js'></script>
      <script src='vendor/bootstrap/js/bootstrap.bundle.min.js'></script>

      <!-- Core plugin JavaScript-->
      <script src='vendor/jquery-easing/jquery.easing.min.js'></script>
      <!-- Custom scripts for all pages-->
      <script src='js/sb-admin-2.min.js'></script>
      <!-- Angular -->
      <script src='vendor/angular/angular.js'></script>
      <!-- Jquery -->
      <script src='js/animated-esw.js'></script>
      <script src='js/fsm-esw.js'></script>
      <script src='js/jquery-3.5.1.js'></script>

      <!-- Page level plugins -->
      <script src='vendor/chart.js/Chart.min.js'></script>

      <!-- Page level custom scripts -->
      <script src='js/demo/chart-area-demo.js'></script>
      <script src='js/demo/chart-pie-demo.js'></script>
      
   </body>
   
</html>

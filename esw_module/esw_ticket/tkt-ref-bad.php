<?php 

echo "<div class='card text-center'>
         <div class='card-header ticket-bg-head-bad'>
            <span class='ticket-title'>Penalty X1</span>
         </div>
         <div class='card-body'>
            <span class='ticket-sub-title'>-5 WAR COIN</span>
            <span class='ticket-body'><br>You've been granted a penalty resulting to the last war report you've made.<br>You're fairplay indicator is also impacted.<br><br></span>
            <a href='#' class='btn-esw'>read the guidelines</a>
            &nbsp;&nbsp;
            <a href='#' class='btn-ticket'>contest the decision</a>
         </div>
         <div class='card-footer ticket-bg-footer-bad'>
            <div class='container'>
               <div class='row'>
                  <div class='col-sm'>
                     <span class='ticket-footer'>@Knuckles</span>
                  </div>
                  <div class='col-sm'>
                     <span class='ticket-footer'>04/01/2021 15:00</span>
                  </div>
                  <div class='col-sm'>
                     <span class='ticket-footer'>#145857</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
     "
?>
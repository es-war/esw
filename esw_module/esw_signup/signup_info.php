<?php

echo "<body id='page-top' class='bg-signup'>

         <div class='wrapper'>
            
            <div class='row d-flex justify-content-center row-signup-top'>
               <span class='signup-header-text row-signup-mid'>
                  DO YOU WANT TO BE A PART OF A TEAM ?
               </span>
            </div>
            
            <div class='row d-flex justify-content-center'>
            
               <div class='col-sm-6 col-lg-4'>
                  
                  <form method='post' action='index.php?action=3&ack=1' id='signup_form'>
                  
                     <div class='row row-signup-mid'>
                        <div class='col-sm'>
                           <span id='firstname-text' class='signup-text'>&nbsp;&nbsp;&nbsp;&nbsp;First name</span>&nbsp;
                           <input id='firstname-input' name='first_name' type='text' class='form-control' placeholder='First name' onfocusout='empty_signup()'>
                        </div>
                        <div class='col-sm'>
                           <span id='secondname-text' class='signup-text'>&nbsp;&nbsp;&nbsp;&nbsp;Second name</span>&nbsp;
                           <input id='secondname-input' name='second_name'type='text' class='form-control' placeholder='Second name' onfocusout='empty_signup()'>
                        </div>
                     </div>
                     
                     <div class='row row-signup-mid'>
                        <div class='col-sm'>
                           <span id='mail-text' class='signup-text'>&nbsp;&nbsp;&nbsp;&nbsp;Email</span>&nbsp;
                           <input id='mail-input' name='user_email' type='email' class='form-control' id='exampleInputEmail1' aria-describedby='emailHelp' placeholder='Email'>
                        </div>
                     </div>
                     
                     <div class='row row-signup-mid'>
                        <div class='col-sm'>
                           <span id='mobile-text' class='signup-text'>&nbsp;&nbsp;&nbsp;&nbsp;Mobile number</span>&nbsp;
                           <input id='mobile-input' name='user_mobile' type='text' class='form-control' placeholder='Mobile number' onfocusout='numeric_signup()'>
                        </div>
                        <div class='col-sm'>
                           <span id='country-text' class='signup-text'>&nbsp;&nbsp;&nbsp;&nbsp;Country</span>&nbsp;
                           <select id='country-input' name='user_country' class='form-control select-signup'>
                              <option>France</option>
                              <option>Great Britain</option>
                              <option>Germany</option>
                           </select>
                        </div>
                     </div>
                     
                     <div class='row row-signup-mid'>
                        <div class='col-sm'>
                           <span id='birthday-text' class='signup-text'>&nbsp;&nbsp;&nbsp;&nbsp;Birthday</span>&nbsp;
                           <input id='birthday-input' name='user_birthday' type='text' class='form-control' placeholder='Birthday DD/MM/YYYY' onfocusout='birthdate_signup()'>
                        </div>
                        <div class='col-sm'>
                           <span id='gender-text' class='signup-text'>&nbsp;&nbsp;&nbsp;&nbsp;Gender</span>&nbsp;
                           <select id='gender-input' name='user_gender' class='form-control select-signup'>
                              <option>Male</option>
                              <option>Female</option>
                              <option>Other</option>
                           </select>
                        </div>
                     </div>
                     
                     <div class='row checkbox-signup-row'>
                        <div class='col-sm'>
                           <br>
                           <input type='checkbox' class='form-check-input' id='policy_terms_signup'>
                           <span id='policy_terms_text' class='checkbox-signup'>
                              i agree with the <a href='' class='signup-link'>Policy & Terms</a>
                           </span>
                           <span id='policy_terms_fail' class='checkbox-signup-fail'>
                              &nbsp;
                           </span>
                        </div>
                     </div>

                     <div class='row checkbox-signup-row'>
                        <div class='col-sm'>
                           <input type='checkbox' class='form-check-input' id='judgement_arbitration_signup'>
                           <span id='judgement_arbitration_text' class='checkbox-signup'>
                              i agree with the <a href='' class='signup-link'>Judgement & Arbitration rules</a>
                           </span>
                           <span id='judgement_arbitration_fail' class='checkbox-signup-fail'>
                              &nbsp;
                           </span>
                        </div>
                     </div>

                     <div class='row checkbox-signup-row'>
                        <div class='col-sm'>
                           <input type='checkbox' class='form-check-input' id='banking_cash_signup'>
                           <span id='banking_cash_text' class='checkbox-signup'>
                              i agree with the <a href='' class='signup-link'>Banking & Cash handling rules</a>
                           </span>
                           <span id='banking_cash_fail' class='checkbox-signup-fail'>
                              &nbsp;
                           </span>
                        </div>
                     </div>

                     <div class='row row-signup-mid row-signup-button'>
                        <div class='col-sm text-center'>
                           <button type='button' id='signup_btn' class='btn-signup'>sign up</button>
                        </div>
                     </div>

                     <div class='row row-signup-mid row-signup-button'>
                        <div class='col-sm text-center'>
                           <a href='index.php' class='signup-link'>cancel and return on the website</a>
                        </div>
                     </div>
                  </form>
               </div>
            </div>  
         </div>";
?>
<?php
require_once("esw_system/esw_core/sql_function.php");
require_once("esw_system/esw_core/token_handler.php");
require_once("esw_system/esw_core/mail_parser.php");

if(!isset($_REQUEST['ack']))
{
   //if no ack defined UB
   include 'esw_module/esw_error/error_101.php';
}
else
{
   switch($_REQUEST['ack']) {
      
      //ack of the signup registration
      case 1:
         
         //if no info from sign up page go error
         if(isset($_POST['first_name']) &&
            isset($_POST['second_name']) &&
            isset($_POST['user_email']) &&
            isset($_POST['user_mobile']) &&
            isset($_POST['user_country']) &&
            isset($_POST['user_birthday']) &&
            isset($_POST['user_gender']))
         {
            $activation = 0;
            $code = 0;
            
            //CHECK IF THE USER EXIST
            $result = TODB::get_signup_info($_POST['user_email']);
            
            if($result == 0) {
               require_once("esw_system/esw_core/hash_signup.php");
               $sign_up_pwd = new pwd_mgt();

               $code = $sign_up_pwd->signup_pw(htmlspecialchars($_POST['first_name']), 
                                               htmlspecialchars($_POST['second_name']), 
                                               htmlspecialchars($_POST['user_email']), 
                                               htmlspecialchars($_POST['user_mobile']),
                                               htmlspecialchars($_POST['user_country']), 
                                               htmlspecialchars($_POST['user_birthday']), 
                                               htmlspecialchars($_POST['user_gender']));
                                               
               $email_code = $sign_up_pwd->email_pw($code);
               $email_sha3 = $sign_up_pwd->email_sha3($email_code);
               
               //ADD THE USER
               TODB::add_user($_POST['first_name'], $_POST['second_name'], $email_sha3, $_POST['user_email'], $_POST['user_mobile'], $_POST['user_country'], $_POST['user_birthday'], $_POST['user_gender'], 1, $code, $email_code, 0);
               //CREATE USER TOKEN
               TKN::create_tkn(base64_encode($code), 3600);
               
               //mailing email code part
               require_once("esw_system/esw_core/newsletter.php");
               $mail = new esw_mod_mailing();
               $b64_mail = base64_encode(htmlspecialchars($_POST['user_email']));
               //define content
               $mail->set_mail_subject("🎫 Welcome in ESW : Account activation");
               
               //DEFINE MAIL PARSER TEMPLATE
               $data = array("@@ACTIVATION_CODE@@" => $email_code, 
                             "@@PLAYER_NAME@@" => $_POST['first_name'],
                             "@@HREF_LINK@@" => $GLOBALS['dns']."/index.php?action=3&ack=2&pk1=".$b64_mail."&pk2=".$code,
                             "@@PATH_IMG@@" => "activation");
                              
               //PARSE MAIL TEMPLATE
               $mail->set_mail_message(MPARSER::parse_mail($data, "esw_system/esw_core/mail_template/activation/index.html"));
               
               //destination
               $mail->set_mail_from("ESW - Activation <ticketing@esw.com>");
               $mail->set_mail_reply("support@esw.com");
               $mail->set_mail_to(htmlspecialchars($_POST['user_email']));
               
               //timing & follow
               $mail->set_mail_interval("0");
               $mail->activate_tracing();
               
               //prepare for CRON
               $mail->prepare_mail();
            } 
            else {
               //retrieve the existing sign up code
               $code = $result[0];
               $activation = $result[1];
            }
            
            if($activation == 1)
               include 'signup_ack_already.php';
            else
               include 'signup_ack_ok.php';
         }
         else
            include 'error_101.php';
         
      break;
      
      //ack of the email validation
      case 2:
      
         if(isset($_REQUEST['pk1']) 
         && isset($_REQUEST['pk2']) 
         && TKN::valid_tkn(base64_encode($_REQUEST['pk2']))) {
            
            $code = htmlspecialchars($_REQUEST['pk2']);
            $mail = base64_decode(htmlspecialchars($_REQUEST['pk1']));

            //GET ID AND ACTIVATION
            $return = TODB::get_email_info($mail, $code);
            
            if($return != 0) {
               $user_id = $return[0];
               $activated = $return[1];
               
               if($activated == 0) {
                  //UPDATE ACCOUNT ACTIVATION
                  TODB::upd_activation_info($user_id);
                  //CONSUME THE TOKEN
                  TKN::consume_tkn(base64_encode($_REQUEST['pk2']));
                  
                  include 'signup_activation_ok.php';
               }
               //already activated
               else {
                  include 'signup_ack_already.php';
               }
            }
            else
               include 'esw_module/esw_error/error_100.php';
         }
         else
            include 'esw_module/esw_error/error_101.php';
      
      break;
      
      default:
      break;
   }
}

?>
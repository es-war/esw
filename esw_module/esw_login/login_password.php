<?php

echo "<div class='row d-flex justify-content-center row-signup-top'>
         <span class='signup-header-text row-signup-mid'>
            PASSWORD LOST ?
         </span>
      </div>
      
      <div class='row d-flex justify-content-center'>
      
         <div class='col-sm-6 col-lg-4'>
            
            <form method='post' action='index.php?action=5&ack=1' id='lostpw_form'>
            
               <div class='row row-signup-mid'>
                  <div class='col-sm'>
                     <span id='mail_pw_lost-text' class='signup-text'>&nbsp;&nbsp;&nbsp;&nbsp;Email</span>&nbsp;
                     <input id='mail_pw_lost-input' name='mail_pw_lost' type='email' class='form-control' placeholder='Email'>
                  </div>
               </div>
               
               <div class='row row-signup-mid row-signup-button'>
                  <div class='col-sm text-center'>
                     <button type='button' id='pwlost_btn' class='btn-signup'>mail me new password</button>
                  </div>
               </div>

               <div class='row row-signup-mid row-signup-button'>
                  <div class='col-sm text-center'>
                     <a href='index.php' class='signup-link'>cancel and return on the website</a>
                  </div>
               </div>
            </form>
         </div>
      </div>";
?>
<?php
require_once("esw_system/esw_core/sql_function.php");
require_once("esw_system/esw_core/token_handler.php");

if(!isset($_REQUEST['ack']))
{
   //if no ack defined UB
   include 'esw_module/esw_error/error_101.php';
}
else
{
   switch($_REQUEST['ack']) {
      
      //password reset
      case 1:
         
         if(isset($_POST['mail_pw_lost']))
         {
            //the account has been found
            if(TODB::get_id($_POST['mail_pw_lost']) >= 0) 
            {
               //mailing email code part
               require_once("esw_system/esw_core/newsletter.php");
               $mail = new esw_mod_mailing();
               
               $b64_mail = base64_encode($_POST['mail_pw_lost']);
               
               //DEFINE UNIQUE HASH
               $unique_hash = TKN::create_hash($b64_mail);
               //CREATE USER TOKEN
               TKN::create_tkn($unique_hash, 3600);
               
               $formated_mail_msg = "<a href='".$GLOBALS['dns']."/index.php?action=5&ack=2&pk1=".$b64_mail."&pk2=".$unique_hash."&reason=RESET%20PASSWORD'>Click here to reset</a>";
               
               //define content
               $mail->set_mail_subject("ESW : You\'ve asked for a password reset");
               $mail->set_mail_message($formated_mail_msg);
               
               //destination
               $mail->set_mail_from("ESW - Password reset <ticketing@esw.com>");
               $mail->set_mail_reply("support@esw.com");
               $mail->set_mail_to($_POST['mail_pw_lost']);
               
               //timing & follow
               $mail->set_mail_interval("0");
               $mail->activate_tracing();
               
               //prepare for CRON
               $mail->prepare_mail();
               
               include 'login_mail_sent.php';
            }
            else 
               include 'login_err_no_user.php';
         }
         //in case of no info
         else
            include 'esw_module/esw_error/error_101.php';
         
      break;
      
      //password reset validation
      case 2:
         
         if(isset($_REQUEST['pk1']) 
         && TKN::valid_tkn($_REQUEST['pk2']))
         {
            //after password change
            if(isset($_REQUEST['end']) && isset($_POST['password_new']))
            {
               require_once("esw_system/esw_core/hash_signup.php");
               $pwd = new pwd_mgt();
               
               $mail = base64_decode($_REQUEST['pk1']);
               $password = $pwd->email_sha3($_POST['password_new']);
               
               //UPDATE PASSWORD
               TODB::upd_password($mail, $password);
               //CONSUME THE TOKEN
               TKN::consume_tkn($_REQUEST['pk2']);
               
               include 'login_password_changed.php';
            }
            //ask for new password 
            else 
               include 'login_change_password.php';
         }
         else 
            include 'esw_module/esw_error/error_101.php';
         
      break;
      
      //login
      case 3:
         
         if(isset($_POST['user_email']) && isset($_POST['user_password'])) {
            
            //CHECK USER INFO FROM LOGIN
            $result = TODB::get_login_info($_POST['user_email']);
            
            if($result > 0) {
               require_once("esw_system/esw_core/hash_signup.php");
               $pwd = new pwd_mgt();
               
               //set session ID data
               $password = $pwd->email_sha3($_POST['user_password']);
               
               //if the account is not activated ask him to activate his account by reading his mail
               switch($result[3]) {
                  case 0:
                     include 'login_ack_ko.php';
                  break;
                  //if this is the first time he log in
                  case 1:
                     if($password == $pwd->email_sha3($result[4])) {
                        
                        session_handler::setSes($result[0], $result[1], $result[2], $GLOBALS['session_lifetime']);
                        
                        $_REQUEST['reason'] = "UPON FIRST LOGIN PLEASE CHANGE PASSWORD";
                        $_REQUEST['pk1'] = base64_encode($result[1]);
                        //DEFINE UNIQUE HASH
                        $_REQUEST['pk2'] = TKN::create_hash($_REQUEST['pk1']);
                        //CREATE USER TOKEN
                        TKN::create_tkn($_REQUEST['pk2'], 3600);
                        
                        include 'login_change_password.php';
                     } 
                     else {
                        include 'login_ack_ko.php';
                     }                      
                  break;
                  //normal log in
                  case 2:
                     if($password == $result[2]) {
                        session_handler::setSes($result[0], $result[1], $result[2], $GLOBALS['session_lifetime']);
                        include 'login_ack_ok.php';
                     }
                     else {
                        include 'login_ack_ko.php';
                     }
                  break;
                  default:
                     include 'login_ack_ko.php';
                  break;
               }
            }
            else 
               include 'login_ack_ko.php';
         }
         else
            include 'error_101.php';
      break;
   }
}

?>
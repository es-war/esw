<?php 
/*
 * Copyright (c) 2019 ESWAR and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * Please contact ESWAR main developper : redtsubasa1 - RT1
 * or visit www.es-war.com if you need additional information or have any
 * questions.
 */
 
require_once("esw_system/esw_core/sql_function.php");

class esw_mod_mailing {
    
   private $to;
   private $from;
   private $reply_to;
   private $version;
   private $content_type;
   private $message;
   private $subject;
   private $header;
   private $activate_tracing;
   private $tracing_website;
   private $k1_arr;
   private $interval;

   const TOMOD = '@@MAIL@@';

   const VER = 'MIME-Version: 1.0\r\n';
   const CTT_UTF8 = 'Content-type: text/html; charset=utf-8\r\n';
   const CTT_8859_1 = 'Content-Type: text/html; charset=ISO-8859-1\r\n';
   const REPLY = 'Reply-To: @@MAIL@@\r\n';
   const FROM = 'From: @@MAIL@@\r\n';
    
   public function __construct($mail_version = 'MIME-Version: 1.0\r\n', $mail_content_type = 'Content-type: text/html; charset=utf-8\r\n', $mail_reply_to = 'Reply-To: @@MAIL@@\r\n', $mail_from = 'From: @@MAIL@@\r\n', $act_trc = 0) {
      $this->from = $mail_from;
      $this->reply_to = $mail_reply_to;
      $this->version = $mail_version;
      $this->content_type = $mail_content_type;
      $this->activate_tracing = $act_trc;
      $this->k1_arr = array();
   }
    
   public function set_mail_from($mail) {
      $this->from = str_replace($this::TOMOD, $mail, $this->from);
   }

   public function set_mail_reply($mail) {
      $this->reply_to = str_replace($this::TOMOD, $mail, $this->reply_to);
   }
    
   /** 
   *  example 1 : knucklesskills@gmail.com
   *  example 3 : knucklesskills@gmail.com;knucklesskills@gmail.com;knucklesskills@gmail.com
   */
   public function set_mail_to($mail) {
      $this->to = $mail;
   }

   public function set_mail_message($html) {
      $this->message = $html;
   }

   public function activate_tracing() {
      $this->activate_tracing = 1;
      if(strpos($this->to, ";") === false) {
         array_push($this->k1_arr, md5($this->to));
      } 
      else {
         $tmp = explode(";", $this->to);
         for($i = 0; $i < count($tmp); ++$i) {
            array_push($this->k1_arr, md5($tmp[$i]));
         }
      }
   }
    
   public function set_mail_subject($txt) {
      $this->subject = $txt;
   }
    
    /** 
     *  in minutes
     *  example 1 (it will send 1 mail at NOW + 1 day) : 1440
     *  example 3 (it will send 3 mails at NOW + 1 day, NOW + 1 month, NOW + 6 months) : 1440;43200;259200
     */
   public function set_mail_interval($interval) {
      $this->interval = $interval;
   }
    
   public function gen_mail() {
      //conformity checks
      if(empty($this->to)) return 0;
      if(empty($this->subject)) return 0;

      $headers = 'MIME-Version: 1.0' . "\r\n";  
      $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
      $headers .= $this->from .
                  $this->reply_to .  
                  "X-Mailer: PHP/" . phpversion();
                  
      $this->header = $headers;
   }
    
   public function prepare_mail() {
        
        $this->gen_mail();
        $this->save_to_file();
        
        //get the next send timing
        $next_send_timing = explode(";", $this->interval);
        
        $message = bin2hex($this->message);
        
        //SEND THE MAIL TO THE CRON
        TODB::add_mail($this->subject, $this->from, $this->reply_to, $this->header, $this->to, $message, $this->activate_tracing, date("Y-m-d H:i:s"), $next_send_timing[0], $this->interval, 0, 0);
    }
    
   public function save_to_file() {
      $mail_save = "esw_system/esw_core/mail_task/".md5($this->subject)."_to_".md5($this->to).".html";
      
      $fp = fopen($mail_save, 'w');
      fwrite($fp, $this->message);
      fclose($fp);
   }
    
   //TO DEBUG
   public function check_mail() {
      echo "to : ".$this->to."<br>";
      echo "subject : ".$this->subject."<br>";
      echo "message : ".htmlspecialchars($this->message)."<br>";
      echo "header : ".$this->header."<br>";
      echo "tracing_tag : ".$this->tracing_tag."<br>";

      for($i = 0; $i < count($this->k1_arr); ++$i) {
         echo "[".$i."]KEY : ".$this->k1_arr[$i]."<br>";
      }
   }
}
 
?>

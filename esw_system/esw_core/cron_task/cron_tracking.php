<?php 
/*
 * Copyright (c) 2019 ESWAR and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * Please contact ESWAR main developper
 * or visit www.es-war.com if you need additional information or have any
 * questions.
 */

require_once("esw_system/esw_core/sql_function.php");
require_once("esw_system/esw_core/protocol.php");

echo "<center>CRON MAILING -> SERVER</center>";

//SERVER : for this one the component will just act as a server when the recepient will open their mail (esport_mail_tracing)

//check the access.log file
$access_log = file_get_contents($GLOBALS['access_log']);
$str_idx = "esw_system/esw_core/cron_task/tracking/";
$folder_end = "/";
$k_separator = "_";
$k_end = "/";
$extension = ".png";
$k1 = 0;
$k2 = 0;
$k3 = 0;

$mydbsel = new db_select();
$mydbupd = new db_update();

while(strpos($access_log, $str_idx) !== false) {
   $idx = strpos($access_log, $str_idx);
   //reset our keys
   $k1 = 0;
   $k2 = 0;
   $k3 = 0;
   
   //remove before idx
   $access_log = substr($access_log, $idx + strlen($str_idx));
   $folder_path_idx = strpos($access_log, $folder_end);
   
   //never know if the access log is writting
   if($folder_path_idx !== false) {
      
      //extract the folder path
      $folder_path = substr($access_log, 0, $folder_path_idx + strlen($folder_end));
      $qr_folder = $folder_path;
      $k1_idx = strpos($folder_path, $k_separator);

      //valid k1
      if($k1_idx !== false) $k1 = substr($folder_path, 0, $k1_idx);

      //move forward
      $folder_path = substr($folder_path, $k1_idx + strlen($k_separator));
      $k2_idx = strpos($folder_path, $k_separator);
      
      //valid k2
      if($k2_idx !== false) $k2 = substr($folder_path, 0, $k2_idx);
      
      //move forward
      $folder_path = substr($folder_path, $k2_idx + strlen($k_separator));
      $k3_idx = strpos($folder_path, $k_end);
      
      //valid k3
      if($k3_idx !== false) $k3 = substr($folder_path, 0, $k3_idx);
      
      //make one jump if key extraction is corrupted
      if($k1 && $k2 && $k3) {
         //make sure the QR ressource was fetched
         $qr_code = $k1.$k_separator.$k2.$k_separator.$k3.$extension;
         $full_qr_path = $qr_folder.$qr_code;
         
         $ressource_fetched = strpos($access_log, $full_qr_path);
         if($ressource_fetched !== false) {

            $result = TODB::get_tracing($k1, $k2, $k3);
            
            //if the tracking record don't exist we leave
            if($result > 0) {
               //validate that the tracking is at 'not open'
               if($result[0] == 0) {
                  $now_timestamp = new DateTime("now");
                  $nowstamp = $now_timestamp->format("Y-m-d H:i:s");
                  
                  //UPDATE TRACING AS OPENNED
                  TODB::udp_tracing($nowstamp, 1, $k1, $k2, $k3);
               }
            }
         }
      }
   }
}

?>
<?php 
/*
 * Copyright (c) 2019 ESWAR and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * Please contact ESWAR main developper : redtsubasa1 - RT1
 * at : KnucklesSkills@gmail.com
 * or visit www.es-war.com if you need additional information or have any
 * questions.
 */

require_once("esw_system/esw_core/sql_function.php");
require_once("esw_system/esw_library/phpqrcode/qrlib.php");
require_once("esw_system/esw_core/mail_parser.php");

echo "<center>CRON MAILING -> REQUESTER</center>";

//REQUESTER : as this feature the component will check what mail in : esport_mail need to be send

//GET ALL MAIL TO SEND
while($tmp = TODB::get_mail()) {
   
   //our next previsional timing
   $time = new DateTime($tmp['last_send_date']);
   $time->add(new DateInterval('PT'.$tmp['next_send_timing'].'M'));
   $stamp = $time->format('Y-m-d H:i:s');

   //our actual timing
   $now_timestamp = new DateTime("now");
   $nowstamp = $now_timestamp->format('Y-m-d H:i:s');

   //yes we have to send it
   if($time <= $now_timestamp) {
      //explode all the recepient
      $destinators = explode(";", $tmp['mail_to']);

      for($i = 0; $i < count($destinators); ++$i) {
         
         $message = hex2bin($tmp['message']);

         //if the mail have to be traced for analytics
         if($tmp['tracing']) {
            $k1 = md5($tmp['subject']);
            $k2 = md5($destinators[$i]);
            $k3 = md5($nowstamp);

            //counter google proxy
            $dir_name = $k1."_".$k2."_".$k3;

            //prepare the specific tracking folder
            $dir_path = dirname(__FILE__)."/tracking/".$dir_name;
            mkdir($dir_path, 0777);

            //define the tracking QR
            $dir_path .= "/".$dir_name.".png";
            $dir_path_mail = $dir_name."/".$dir_name.".png";

            //generate the tracking QR in the specific folder
            QRcode::png($dir_name, $dir_path, QR_ECLEVEL_M, 4);

            //add the QR img to the mail as tracking element
            $trace_tag = "<img src='".$GLOBALS['dns']."/esw_system/esw_core/cron_task/tracking/".$dir_path_mail."'>";
            
            //DEFINE MAIL PARSER TEMPLATE
            $data = array("@@TRACING@@" => $trace_tag);
            
            $message = MPARSER::next_parse_mail($data, $message);
            
            //ADD THE TRACING
            TODB::add_tracing($k1, $k2, $k3, 0, $nowstamp);
         }
         
         //send mail
         mail($destinators[$i], $tmp['subject'], $message, $tmp['header']);
      }

      //compute new timestamps
      $next_stamp = $tmp['interval_index'] + 1;
      $intervals = explode(";", $tmp['mail_interval']);

      //it will mean that we still have to repeat this mailing
      if($next_stamp <= (count($intervals) - 1))
         //last_send_date become NOW
         TODB::upd_mail($tmp['id'], $nowstamp, $intervals[$next_stamp], $next_stamp, 0);
      else
         //we can close this mailing
         TODB::upd_mail($tmp['id'], $nowstamp, -1, -1, 1);

   }
   
}

?>
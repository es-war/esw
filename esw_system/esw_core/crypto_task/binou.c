#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define BUFFSIZE 128
#define FSIZE 1024
#define ARGSIZE 5
#define EPOCH 32

int main(int argc, char *argv[])
{
   char full[FSIZE] = "";
   char argsize[ARGSIZE][2];
   char epoch[EPOCH] = "";
   int epochsize = 0;
   int fullsize = 0;
   int cursor = 0;
   int totalsize = 0;
   
   char hash1[FSIZE] = "";
   char hash2[FSIZE] = "";
   char fhash[FSIZE] = "";
   
   //file extraction
   {
      FILE* fp;
      char buffer[BUFFSIZE];
      char* filename = "input.txt";
      
      if((fp = fopen("input.txt", "r")) == NULL)
      {
         char bitmap[ARGSIZE * 2] = "";
         
         //read hashed file
         if(fp = fopen("database.txt", "r"))
         {
            while(fgets(buffer, sizeof(buffer), fp) != NULL)
            {
               if(buffer[strlen(buffer) - 1] == '\n')
                  buffer[strlen(buffer) - 1] = '\0';
               
               strcat(full, buffer);
               printf("%s\n", full);
            }
         }

         //popout epoch
         {
            int epoch = ((full[8] - 48) * 10) + (full[9] - 48);
            int itt = ARGSIZE * 2 - 1;
            int jtt = 0;
            for(; itt < (strlen(full)); ++itt) {
               if(itt % 2 && jtt < epoch)
                  hash1[jtt++] = full[itt];
               else if(jtt >= epoch)
                  hash1[jtt++] = full[itt];
            }
         }
//0▒R▒\▒o▒N▒1▒9▒e▒i▒i▒D▒<▒d▒\▒6▒9▒▒▒O▒s▒v▒k▒M▒i▒d▒_▒n▒z▒s▒c▒s▒h▒a▒o▒
         printf("%s\n", hash1);
         printf("size : %d\n", strlen(full));
         
         return 0;
      }
      while(fgets(buffer, sizeof(buffer), fp) != NULL)
      {
         if(buffer[strlen(buffer) - 1] == '\n')
            buffer[strlen(buffer) - 1] = '\0';
         
         //bitmap creation
         if(cursor < ARGSIZE)
         {
            if(strlen(buffer) <= 9)
            {
               argsize[cursor][0] = '0';
               argsize[cursor][1] = strlen(buffer) + 48;
            }
            else
            {
               argsize[cursor][0] = (strlen(buffer) / 10) + 48;
               argsize[cursor][1] = (strlen(buffer) % 10) + 48;
            }
         }
         strcat(full, buffer);
         cursor++;
      }
      fullsize = strlen(full);
      fclose(fp);
   }
   
   //reverse full
   {
      int itt = fullsize - 1, jtt = 0;
      for(; itt >= 0; --itt, ++jtt) {
         hash2[jtt] = full[itt];
      }
   }
   
   //hash part 1 - epoch
   {
      unsigned long epochtime = (unsigned long)time(NULL), buffer;
      int itt = 0;
      for(; epochtime % 10; buffer = epochtime % 10, epochtime /= 10)
      {
         epoch[itt] = buffer + 48;
         itt++;
      }
      
      epoch[0] = 49;
      
      //add 20 random ASCII chars
      int jtt = 0, nrand = 0;
      for(; jtt < 20; nrand = rand() % 100 + 1, ++jtt, ++itt) {
         epoch[itt] = nrand + 48;
      }
      
      epochsize = itt;
      
      //add epoch + ASCII chars to bitmap
      if(epochsize <= 9)
      {
         argsize[cursor][0] = '0';
         argsize[cursor][1] = epochsize + 48;
      }
      else
      {
         argsize[cursor][0] = (epochsize / 10) + 48;
         argsize[cursor][1] = (epochsize % 10) + 48;
      }
      
      totalsize = epochsize + fullsize;
      
      //inject epoch + ASCII chars
      for(itt = 0, jtt = 0; itt < totalsize; ++itt, ++jtt) {
         if(jtt < epochsize) {
            hash1[itt] = epoch[jtt];
            hash1[++itt] = hash2[jtt];
         }
         else
            hash1[itt] = hash2[jtt];
      }
      
      //xor each even char
      for(itt = 0; itt < totalsize; ++itt) {
         if((itt % 2))
            hash1[itt] ^= 0xFF;
      }
   }
   
   cursor++;
   totalsize += ARGSIZE * 2; 
   
   //add the bitmap to the final hash
   {
      int itt = 0, jtt = 0, ktt = 0;
      for(; itt < totalsize; ++itt, ++jtt) {
         if(itt < ARGSIZE * 2) {
            fhash[itt] = argsize[jtt][0];
            fhash[++itt] = argsize[jtt][1];
         }
         else
            fhash[itt] = hash1[ktt++];
      }
   }
   
   //show stuff
   {
      int itt;
      for(itt = 0; itt < ARGSIZE; itt++)
      {
         printf("sise is : %c%c\n", argsize[itt][0], argsize[itt][1]);
      }
      for(itt = 0; itt < epochsize; itt++)
      {
         printf("%c", epoch[itt]);
      }
   }

   printf("\n%s\n%d\n%s\n%s\n", full, epochsize, hash1, fhash);
   
   //save final hash
   {
      FILE * fp;
      fp = fopen("database.txt", "w");
      fprintf(fp, "%s", fhash);
      fclose(fp);
   }
   
   //remove input file
   remove("input.txt");
   
   return 0;
}

<?php

/*
 * Copyright (c) 2019 ESWAR and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * Please contact ESWAR main developper : redtsubasa1 - RT1
 * or visit www.es-war.com if you need additional information or have any
 * questions.
 */

class dyn_cache {
    
    public $meta_array;
    
    public function __construct() {
        /**
         * meta_array[0][0] is the array listing all the objects iterations
         * meta_array[x][0] is the array listing all the data for a given object x
         * meta_array[x][y] is the array listing all the sub data for a given data y at a given object x
         * meta_array[x][y][z] is the value for a given sub data z at a given data y at a given object x
         */
        $this->meta_array = array(array(array()));
    }
    
    //saving object properties in order to save database call time latency
    public function add($object_name, $data_name, $sub_name, $value) {
        
        if(in_array($object_name, $this->meta_array[0][0], TRUE)) {
            
            $object_id = 0;
            for(; $object_name != $this->meta_array[0][0][$object_id++];);
            
            if(in_array($data_name, $this->meta_array[$object_id][0], TRUE)) {
                
                $data_id = 0;
                for(; $data_name != $this->meta_array[$object_id][0][$data_id++];);
                
                if(array_key_exists($sub_name, $this->meta_array[$object_id][$data_id])) {
                    
                    if($this->meta_array[$object_id][$data_id][$sub_name] === $value)
                        return 1; //the data already exist and it's the same
                        
                    return 2; //the data already exist but the value is not the same
                    
                } else {
                    
                    $this->meta_array[$object_id][$data_id][$sub_name] = $value;
                    
                    return 3; //sub name and value added
                }
            } else {
                
                //add the data_name key
                array_push($this->meta_array[$object_id][0], $data_name);
                
                //add the data_name, sub_name and value
                $new_data = array($sub_name => $value);
                array_push($this->meta_array[$object_id], $new_data);
                
                return 4; //data name, sub name and value added
            }
        } else {
            
            //add the object_name key and retrieve his key
            array_push($this->meta_array[0][0], $object_name);
            $object_id = count($this->meta_array[0][0]);
            
            //add the data_name key register
            $new_object = array(array($data_name));
            array_push($this->meta_array, $new_object);
            
            //add the data_name, sub_name and value
            $new_data = array($sub_name => $value);
            array_push($this->meta_array[$object_id], $new_data);
            
            return 5; //object name, data name, sub name and value added
        }
    }
    
    //check if the given sub name exist and if we have a value for it
    public function exi($object_name, $data_name, $sub_name) {
        
        if(in_array($object_name, $this->meta_array[0][0], TRUE)) {
            
            $object_id = 0;
            for(; $object_name != $this->meta_array[0][0][$object_id++];);
            
            if(in_array($data_name, $this->meta_array[$object_id][0], TRUE)) {
                
                $data_id = 0;
                for(; $data_name != $this->meta_array[$object_id][0][$data_id++];);
                
                if(array_key_exists($sub_name, $this->meta_array[$object_id][$data_id])) {
                    
                    if(!empty($this->meta_array[$object_id][$data_id][$sub_name]))
                        return TRUE;
                }
            }
        }
        
        return FALSE; //no data found
    }
    
    //replace an existing value
    public function rep($object_name, $data_name, $sub_name, $value) {
        
        if(in_array($object_name, $this->meta_array[0][0], TRUE)) {
            
            $object_id = 0;
            for(; $object_name != $this->meta_array[0][0][$object_id++];);
            
            if(in_array($data_name, $this->meta_array[$object_id][0], TRUE)) {
                
                $data_id = 0;
                for(; $data_name != $this->meta_array[$object_id][0][$data_id++];);
                
                if(array_key_exists($sub_name, $this->meta_array[$object_id][$data_id])) {
                    
                    $this->meta_array[$object_id][$data_id][$sub_name] = $value;
                    return TRUE;
                }
            }
        }
        
        return FALSE; //no data found
    }
    
    //check and if available retrieve the value of the object->data->subdata
    public function get($object_name, $data_name, $sub_name) {
        
        if(in_array($object_name, $this->meta_array[0][0], TRUE)) {
            
            $object_id = 0;
            for(; $object_name != $this->meta_array[0][0][$object_id++];);
            
            if(in_array($data_name, $this->meta_array[$object_id][0], TRUE)) {
                
                $data_id = 0;
                for(; $data_name != $this->meta_array[$object_id][0][$data_id++];);
                
                if(array_key_exists($sub_name, $this->meta_array[$object_id][$data_id]))
                    return $this->meta_array[$object_id][$data_id][$sub_name];
            }
        }
        return -1; //no data found
    }
}

?>
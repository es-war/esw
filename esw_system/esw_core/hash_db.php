<?php
/*
 * Copyright (c) 2019 ESWAR and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * Please contact ESWAR main developper : redtsubasa1 - RT1
 * or visit www.es-war.com if you need additional information or have any
 * questions.
 */

class Binou 
{ 
   private static $host = ""; 
   private static $user = ""; 
   private static $pass = ""; 
   private static $data = ""; 
   
   public static function p() { 
      return self::$pass; 
   } 

   public static function u() { 
      return self::$user; 
   } 
   
   public static function d() { 
      return self::$data; 
   } 
   
   public static function h() { 
      return self::$host; 
   } 
   
   public static function decode($path) {
      
      $full = ""; 
      $epoch; 
      $hash1 = "";

      if($fh = fopen($path, 'r')) { 
         while(!feof($fh)) { 
            $line = fgets($fh); 
            $full .= $line; 
         } 
         
         $b2 = ($full[4] * 10) + $full[5];
         $b3 = ($full[6] * 10) + $full[7]; 
         
         fclose($fh); 
         
         $b0 = ($full[0] * 10) + $full[1]; 
      } 

      $b1 = ($full[2] * 10) + $full[3]; 
      $epoch = ($full[8] * 10) + $full[9]; 
      $itt = 5 * 2; 
      $jtt = 0;
      
      for(; $itt < (strlen($full)); ++$itt) 
         if(($itt % 2 && $jtt < $epoch) || ($jtt >= $epoch)) 
            $hash1[$jtt++] = $full[$itt];
         
      for($itt = 0; $itt < strlen($hash1); ++$itt) 
         if((($itt >= $epoch) && ($itt % 2)) || ($itt < $epoch)) 
            $hash1[$itt] = chr(ord($hash1[$itt]) ^ 0xFF); 
         
      self::$data = strrev(substr($hash1, 0, $b3)); 
      self::$pass = strrev(substr($hash1, $b3, $b2)); 
      self::$user = strrev(substr($hash1, ($b2 + $b3), $b1)); 
      self::$host = strrev(substr($hash1, ($b1 + $b2 + $b3), $b0)); 
   } 
}
?>
<?php

/*
 * Copyright (c) 2019 ESWAR and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * 
 * Please contact ESWAR main developper : redtsubasa1 - RT1
 * or visit www.es-war.com if you need additional information or have any
 * questions.
 */

//composition of the cookie table : 
$GLOBALS['DB_TABLE_CK'] = "esport_cookie";
$GLOBALS['DB_TABLE_CK_USER'] = "ck_user";
$GLOBALS['DB_TABLE_CK_HASH'] = "ck_hash";
$GLOBALS['DB_TABLE_CK_MESH'] = "ck_mesh";
$GLOBALS['DB_TABLE_CK_CREA'] = "ck_creation";
$GLOBALS['DB_TABLE_CK_DURA'] = "ck_durability";

//composition of the user table :
$GLOBALS['DB_TABLE_US'] = "esport_user";
$GLOBALS['DB_TABLE_US_FIRST'] = "first_name";
$GLOBALS['DB_TABLE_US_SECOND'] = "second_name";
$GLOBALS['DB_TABLE_US_NICK'] = "user_nickname";
$GLOBALS['DB_TABLE_US_MAIL'] = "user_email";
$GLOBALS['DB_TABLE_US_MOBILE'] = "user_mobile";
$GLOBALS['DB_TABLE_US_COUNTRY'] = "user_country";
$GLOBALS['DB_TABLE_US_BIRTHDAY'] = "user_birthday";
$GLOBALS['DB_TABLE_US_GENDER'] = "user_gender";
$GLOBALS['DB_TABLE_US_PASS'] = "user_password";
$GLOBALS['DB_TABLE_US_POWER'] = "user_power";
$GLOBALS['DB_TABLE_US_SIGN_CODE'] = "sign_up_code";
$GLOBALS['DB_TABLE_US_EMAIL_CODE'] = "email_code";
$GLOBALS['DB_TABLE_US_ACTIVATED'] = "activated";

//composition of the token table:
$GLOBALS['DB_TABLE_TOKEN'] = "esport_token";
$GLOBALS['DB_TABLE_TOKEN_HASH'] = "hash";
$GLOBALS['DB_TABLE_TOKEN_STATUS'] = "status";
$GLOBALS['DB_TABLE_TOKEN_OTIME'] = "open_time";
$GLOBALS['DB_TABLE_TOKEN_VTIME'] = "validity_time";
$GLOBALS['DB_TABLE_TOKEN_CTIME'] = "close_time";

//composition of the tracing table:
$GLOBALS['DB_TABLE_TRACING'] = "esport_mail_tracing";
$GLOBALS['DB_TABLE_TRACING_K1'] = "k1";
$GLOBALS['DB_TABLE_TRACING_K2'] = "k2";
$GLOBALS['DB_TABLE_TRACING_K3'] = "k3";
$GLOBALS['DB_TABLE_TRACING_STATUS'] = "status";
$GLOBALS['DB_TABLE_TRACING_SDATE'] = "send_date";
$GLOBALS['DB_TABLE_TRACING_ODATE'] = "open_date";

//composition of the mailing table:
$GLOBALS['DB_TABLE_MAILING'] = "esport_mail";
$GLOBALS['DB_TABLE_MAILING_SUB'] = "subject";
$GLOBALS['DB_TABLE_MAILING_FROM'] = "mail_from";
$GLOBALS['DB_TABLE_MAILING_REP'] = "reply_to";
$GLOBALS['DB_TABLE_MAILING_HEAD'] = "header";
$GLOBALS['DB_TABLE_MAILING_TO'] = "mail_to";
$GLOBALS['DB_TABLE_MAILING_MSG'] = "message";
$GLOBALS['DB_TABLE_MAILING_TRC'] = "tracing";
$GLOBALS['DB_TABLE_MAILING_SDATE'] = "last_send_date";
$GLOBALS['DB_TABLE_MAILING_STIME'] = "next_send_timing";
$GLOBALS['DB_TABLE_MAILING_INTER'] = "mail_interval";
$GLOBALS['DB_TABLE_MAILING_STAT'] = "status";
$GLOBALS['DB_TABLE_MAILING_INTERIDX'] = "interval_index";

?>
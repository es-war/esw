<?php

/*
 * Copyright (c) 2019 ESWAR and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * Please contact ESWAR main developper : redtsubasa1 - RT1
 * or visit www.es-war.com if you need additional information or have any
 * questions.
 */

require_once("esw_system/esw_core/sql_function.php");

class TKN {
   
   //CREATE A UNIQUE HASH
   //@param data string
   //@return sha1 of [data + current time]
   public static function create_hash($data) {
      
      $now_timestamp = new DateTime("now");
      $now_timestamp = $now_timestamp->format('Y-m-d H:i:s');
      $unique_data = hash('sha1', (htmlspecialchars($data).$now_timestamp));
      
      return $unique_data;
   }
   
   //CREATE TOKEN
   //@param hash string
   //@param validity_time string seconds
   public static function create_tkn($hash, $validity_time) {
      
      $now_timestamp = new DateTime("now");
      $close_time = $now_timestamp;
      
      $open_time = $now_timestamp->format('Y-m-d H:i:s');
      $close_time->add(new DateInterval('PT'.$validity_time.'M'));
      $close_time = $close_time->format('Y-m-d H:i:s');
   
      TODB::add_token($hash, 0, $open_time, $validity_time, $close_time);
   }
   
   //REMOVE TOKEN
   //@param hash string
   public static function delete_tkn($hash) {
      
      TODB::remove_token($hash);
   }
   
   //CHECK IF TOKEN VALID
   //@param hash string
   //@return 1 valid / 0 consumned or error
   public static function valid_tkn($hash) {
      
      return TODB::valid_token($hash);
   }
   
   //REMOVE TOKEN
   //@param hash string
   //@param close_time string
   //@return 1 if burned / 0 if already burned or error
   public static function consume_tkn($hash) {
      
      $now_timestamp = new DateTime("now");
      $close_time = $now_timestamp->format('Y-m-d H:i:s');
      
      return TODB::burn_token($hash, $close_time);
   }
   
   //CHECK TOKEN
   //@param hash string
   //@return 1 exist / 0 null
   public static function check_tkn($hash) {
      
      return TODB::check_token($hash);
   }
   
}
 
?>

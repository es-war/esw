<?php 
/*
 * Copyright (c) 2019 ESWAR and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * Please contact ESWAR main developper : redtsubasa1 - RT1
 * or visit www.es-war.com if you need additional information or have any
 * questions.
 */
 
//parser dictionnary : 
function get_player_name($arg) { return $arg; };
function get_activation_code($arg) { return $arg; };
function get_href_activation($arg) { return $arg; };
function get_path_image($arg) { return $GLOBALS['dns']."/esw_system/esw_core/mail_template/".$arg; };
function get_tracing($arg) { return $arg; };

$GLOBALS['PARSER_DICT'] = [
  '@@PLAYER_NAME@@' => 'get_player_name', 
  '@@ACTIVATION_CODE@@' => 'get_activation_code',
  '@@HREF_LINK@@' => 'get_href_activation',
  '@@PATH_IMG@@' => 'get_path_image',
  '@@TRACING@@' => 'get_tracing'
];

//wrapper to make fast call to the tag
class MPARSER {

   //load the appropriate function for the tag with needed data
   //@param tag the @@TAG@@
   //@param data the arg of the foo related to @@TAG@@ retrieval
   //@return result-data
   public static function parse($tag, $data) {
      return call_user_func($GLOBALS['PARSER_DICT'][$tag], $data);
   }
   
   //replace values + format template + return proper
   //@param data : array of "@@PLAYER_NAME@@" => DATA_TO_REPLACE
   //@param content : semi parsed mail
   //@return mail : the full html parsed
   public static function next_parse_mail($data, $content) {
      
      //check idx's
      foreach($data as $key => $value) {

         if(array_key_exists($key, $GLOBALS['PARSER_DICT']) != 1) {
            return -1;
         }
         
         //check all occurences on key in mail template
         $content = str_replace($key, MPARSER::parse($key, $value), $content);
      }
      
      return $content;
   }
   
   //load template + replace values + format template + return proper
   //@param data : array of "@@PLAYER_NAME@@" => DATA_TO_REPLACE
   //@param path : path to the mail template (index.html)
   //@return mail : the full html parsed
   public static function parse_mail($data, $path) {
      
      //fstream
      $content = file_get_contents($path);

      if($content === FALSE) {
         return -1;
      }
      
      //check idx's
      foreach($data as $key => $value) {

         if(array_key_exists($key, $GLOBALS['PARSER_DICT']) != 1) {
            return -1;
         }
         
         //check all occurences on key in mail template
         $content = str_replace($key, MPARSER::parse($key, $value), $content);
      }
      
      return $content;
   }
} 

?>
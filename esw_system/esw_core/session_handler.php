<?php

/*
 * Copyright (c) 2019 ESWAR and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * Please contact ESWAR main developper : redtsubasa1 - RT1
 * or visit www.es-war.com if you need additional information or have any
 * questions.
 */

require_once("esw_system/esw_core/sql_function.php");
require_once("esw_system/esw_core/hash_compute.php");

class session_handler {
   private static $genesis_label = "genesis";
   
   //FSM to handle the session behaviour
   public static function manage() {
      if(session_handler::to_create()) {         
         //session genesis info
         $_SESSION['user_info'] = session_handler::$genesis_label;
         return 0;
      }
      //we do not ask to kill a genesis cookie
      else if(session_handler::is_valid() && session_handler::to_kill()) {
         session_handler::end();
      }
      return -1;
   }
   
   //flush session
   public static function end() {
      $_SESSION['user_info'] = -1;
      session_unset();
   }
   
   //should be created ?
   public static function to_create() {
      if(isset($_SESSION["user_info"]))
         return false;
      return true;
   }
   
   //should be killed ?
   public static function to_kill() {
      if(session_handler::is_valid()) {
         $now_timestamp = new DateTime("now");
         $info_us = json_decode($_SESSION['user_info']);
         //check the standart deviation, if > we take action over the removal
         if(strtotime($now_timestamp->format('Y-m-d H:i:s')) >= $info_us->{'us_signout_at'}) {
            session_handler::end();
            return true;
         }
      }
      return false;
   }
   
   //validate that the session exist and is currently not a genesis one
   private static function is_valid() {
      if(isset($_SESSION["user_info"]) && strcmp($_SESSION["user_info"], session_handler::$genesis_label) !== 0) {
         return true;
      }
      return false;
   }
   
   //if session dead return -1, else the id
   public static function getId() {
      if(session_handler::is_valid()) {
         return json_decode($_SESSION["user_info"])->{'user_id'};
      }
      return -1;
   }
   
   //if user logged and valid return his power, else -1 as not auth
   public static function is_auth() {
      if(session_handler::is_valid()) {
         $checkout_us = json_decode($_SESSION["user_info"]);
         return TODB::get_power($checkout_us->{'user_id'}, $checkout_us->{'user_mail'}, $checkout_us->{'user_pass'});
      }
      return -1;
   }
   
   //when we give a cookie we set it with the login info washed and crypted 
   //+ we give it an extra log time of genesis_lifetime * 4
   public static function setSes($user_id, $user_mail, $user_pass, $lifetime) {
      if(isset($_SESSION["user_info"])) {
         //define the signin time
         $now_timestamp = new DateTime("now");
         $signin_time = $now_timestamp->format('Y-m-d H:i:s');
         
         //add the lifetime seconds to signin time to define the signout time
         $now_timestamp->add(new DateInterval('PT'.$lifetime.'S'));
         $signout_time = $now_timestamp->format('Y-m-d H:i:s');
         
         //data array construct
         $info_us = array('user_id' => $user_id, 
                          'user_mail' => $user_mail, 
                          'user_pass' => $user_pass,
                          'us_validity' => $lifetime,
                          'us_signout_at' => strtotime($signout_time),
                          'us_signin_at' => strtotime($signin_time));
         
         $_SESSION['user_info'] = json_encode($info_us);
         return true;
      }
      return false;
   }
   
   //used only for debugging
   public static function echo() {
      if(session_handler::is_valid()) {
         $info_us = json_decode($_SESSION['user_info']);
         foreach($info_us as $key => $value) {
             echo "<center>".$key." : ".$value."</center><br/>";
         }
         $now_timestamp = new DateTime("now");
         echo "<center>now time : ".strtotime($now_timestamp->format('Y-m-d H:i:s'))."</center><br/>";
      }
      else {
         echo "<br><center>session not valid</center>";
      }
   }
}
 
?>

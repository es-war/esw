<?php
/*
 * Copyright (c) 2019 ESWAR and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * Please contact ESWAR main developper : redtsubasa1 - RT1
 * or visit www.es-war.com if you need additional information or have any
 * questions.
 */

class hash_playground 
{ 
   private $time;
   private $hashtype;
   private $data_cook;
   private $data_secure;
   private $data_clear;
   
   private function hashme($data) {
      switch($this->hashtype) {
         //bubble
         case 1:
            $data = bin2hex($data);
            $tmp_arr = "";
            
            for($i = 0; $i < strlen($data); ++$i) {
               if($i % 2) {
                  $tmp_arr .= $data[$i].$data[$i-1];
               }
            }
            return $tmp_arr;
            
         break;
      }
   }
   
   private function clearme($data) {
      switch($this->hashtype) {
         //bubble
         case 1:
            $tmp_arr = "";
            
            for($i = 0; $i < strlen($data); ++$i) {
               if($i % 2) {
                  $tmp_arr .= $data[$i].$data[$i-1];
               }
            }
            return hex2bin($tmp_arr);
            
         break;
      }
   }
   
   private function mixup($str1, $str2) {
      $size = strlen($str1) + strlen($str2);
      
      //idx runners 
      $itt_str1 = 0;
      $itt_str2 = 0;
      
      for($i = 0; $i < $size; ++$i) {
         
         //echo "runner : ".$i." | str1 runner : ".$itt_str1." | str2 runner : ".$itt_str2."<br>";
         if(!($i % 2)) {
            $this->data_secure .= $str1[$itt_str1++];
         } else {
            $this->data_secure .= $str2[$itt_str2++];
         }
      }
   }
   
   private function unmixup($str) {
      for($i = 0; $i < strlen($str); ++$i) {
         if($i % 2) {
            $this->data_clear .= $str[$i];
         }
      }
   }
   
   public function encrypt($data) { 
      //define hash to use
      $this->hashtype = 1;
      
      //second id data
      $this->data_cook = $this->hashme($data);
      //first id time
      $this->time = bin2hex(str_replace(array(' ', '.'), "", microtime()));
      
      if(strlen($this->time) > strlen($this->data_cook)) {
         $this->time = substr($this->time, 0, strlen($this->data_cook));
      } else {
         while(strlen($this->time) < strlen($this->data_cook)) {
            $this->time = bin2hex($this->time);
         }
         $this->time = substr($this->time, 0, strlen($this->data_cook));
      }

      $this->mixup($this->time, $this->data_cook);
      
      return $this->data_secure;
   } 

   public function decrypt($data) { 
      //define hash to use
      $this->hashtype = 1;
      
      $this->unmixup($data);
      
      echo $this->clearme($this->data_clear);
   } 
   
}
?>
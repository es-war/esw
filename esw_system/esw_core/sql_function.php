<?php

/*
 * Copyright (c) 2019 ESWAR and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * Please contact ESWAR main developper
 * or visit www.es-war.com if you need additional information or have any
 * questions.
 */

require_once("esw_system/esw_core/protocol.php");
require_once("esw_system/esw_core/sql_handler.php");

class TODB {
   
   /** Check token DB_TABLE_TOKEN
    *  @param hash string
    *  @return 1 valid / 0 consumned or error
    */
   public static function valid_token($hash) { 
   
      $hash_pro = htmlspecialchars($hash);
      
      $sql_select = "SELECT id ".
                    "FROM ".$GLOBALS['DB_TABLE_TOKEN']." ".
                    "WHERE ".$GLOBALS['DB_TABLE_TOKEN_HASH']." = '".$hash_pro."' ".
                    "AND ".$GLOBALS['DB_TABLE_TOKEN_STATUS']." = 0;";
      
      $mydbsel = new db_select();
      $mydbsel->exec($sql_select);
      
      if($mydbsel->numrow() == 1)
         return 1;
      
      return 0;
   }
   
   /** Check token DB_TABLE_TOKEN
    *  @param hash string
    *  @return 1 exist / 0 null
    */
   public static function check_token($hash) { 
   
      $hash_pro = htmlspecialchars($hash);
      
      $sql_select = "SELECT id ".
                    "FROM ".$GLOBALS['DB_TABLE_TOKEN']." ".
                    "WHERE ".$GLOBALS['DB_TABLE_TOKEN_HASH']." = '".$hash_pro."';";
      
      $mydbsel = new db_select();
      $mydbsel->exec($sql_select);
      
      if($mydbsel->numrow() == 1)
         return 1;
      
      return 0;
   }
   
   /** Burn token DB_TABLE_TOKEN
    *  @param hash string
    *  @param close_time string
    *  @return 1 if burned / 0 if already burned or error
    */
   public static function burn_token($hash, $close_time) { 
      
      $hash_pro = htmlspecialchars($hash);
      $close_time_pro = htmlspecialchars($close_time);
      
      $sql_select = "SELECT ".$GLOBALS['DB_TABLE_TOKEN_STATUS']." ".
                    "FROM ".$GLOBALS['DB_TABLE_TOKEN']." ".
                    "WHERE ".$GLOBALS['DB_TABLE_TOKEN_HASH']." = '".$hash_pro."' ".
                    "AND ".$GLOBALS['DB_TABLE_TOKEN_STATUS']." = 0;";
      
      $mydbsel = new db_select();
      $mydbsel->exec($sql_select);
      
      if($mydbsel->numrow() == 1 && $mydbsel->fetchconvresults()[0] == 0) {
         
         $update_sql = "UPDATE ".$GLOBALS['DB_TABLE_TOKEN']." ".
                       "SET ".$GLOBALS['DB_TABLE_TOKEN_STATUS']." = 1, ".
                       $GLOBALS['DB_TABLE_TOKEN_CTIME']." = '".$close_time_pro."' ".
                       "WHERE ".$GLOBALS['DB_TABLE_TOKEN_HASH']." = '".$hash_pro."';"; 
         
         $mydbupd = new db_update();
         $mydbupd->exec($update_sql);
         
         return 1;
      }
      return 0;
   }
   
   /** Remove token DB_TABLE_TOKEN
    *  @param hash string
    */
   public static function remove_token($hash) { 
      
      $hash_pro = htmlspecialchars($hash);
      
      $sql_delete = "DELETE FROM ".$GLOBALS['DB_TABLE_TOKEN']."".
                    "WHERE ".$GLOBALS['DB_TABLE_TOKEN_HASH']." = '".$hash_pro."';";
      
      $mydbdel = new db_delete();
      $mydbdel->exec($sql_delete);
   }
   
   /** Create token DB_TABLE_TOKEN
    *  @param hash string
    *  @param status int
    *  @param open_time string
    *  @param validity_time string
    *  @param close_time string
    */
   public static function add_token($hash, $status, $open_time, $validity_time, $close_time) { 
      
      $hash_pro = htmlspecialchars($hash);
      $status_pro = htmlspecialchars($status);
      $open_time_pro = htmlspecialchars($open_time);
      $validity_time_pro = htmlspecialchars($validity_time);
      $close_time_pro = htmlspecialchars($close_time);
      
      $sql_insert = "INSERT INTO ".$GLOBALS['DB_TABLE_TOKEN']."".
                    "(".$GLOBALS['DB_TABLE_TOKEN_HASH'].", ".
                        $GLOBALS['DB_TABLE_TOKEN_STATUS'].", ".
                        $GLOBALS['DB_TABLE_TOKEN_OTIME'].", ".
                        $GLOBALS['DB_TABLE_TOKEN_VTIME'].", ".
                        $GLOBALS['DB_TABLE_TOKEN_CTIME'].") ".
                   "VALUES ('".$hash_pro."', ".
                           "".$status_pro.", ".
                           "'".$open_time_pro."', ".
                           "'".$validity_time_pro."', ".
                           "'".$close_time_pro."');";
      
      $mydbins = new db_insert();
      $mydbins->exec($sql_insert);
   }
   
   /** Check if tracing need to be done
    *  @return array(DB_TABLE_MAILING_SUB, DB_TABLE_MAILING_FROM, DB_TABLE_MAILING_REP, DB_TABLE_MAILING_HEAD, DB_TABLE_MAILING_TO, DB_TABLE_MAILING_MSG, DB_TABLE_MAILING_TRC, DB_TABLE_MAILING_SDATE, DB_TABLE_MAILING_STIME, DB_TABLE_MAILING_INTER, DB_TABLE_MAILING_INTERIDX)
    */
   public static function udp_tracing($open_date, $status, $k1, $k2, $k3) { 
      
      $open_date_pro = htmlspecialchars($open_date);
      $status_pro = htmlspecialchars($status);
      $k1_pro = htmlspecialchars($k1);
      $k2_pro = htmlspecialchars($k2);
      $k3_pro = htmlspecialchars($k3);

      $update_sql = "UPDATE ".$GLOBALS['DB_TABLE_TRACING']." SET ".
                              $GLOBALS['DB_TABLE_TRACING_ODATE']." = '".$open_date_pro."', ".
                              $GLOBALS['DB_TABLE_TRACING_STATUS']." = ".$status_pro." ".
                    "WHERE ".$GLOBALS['DB_TABLE_TRACING_K1']." = '".$k1_pro."' AND ".
                             $GLOBALS['DB_TABLE_TRACING_K2']." = '".$k2_pro."' AND ".
                             $GLOBALS['DB_TABLE_TRACING_K3']." = '".$k3_pro."' AND ".
                             $GLOBALS['DB_TABLE_TRACING_STATUS']." = 0;";
            
      $mydbupd = new db_update();
      $mydbupd->exec($update_sql);
   }
   
   /** Check if tracing need to be done
    *  @return array(DB_TABLE_MAILING_SUB, DB_TABLE_MAILING_FROM, DB_TABLE_MAILING_REP, DB_TABLE_MAILING_HEAD, DB_TABLE_MAILING_TO, DB_TABLE_MAILING_MSG, DB_TABLE_MAILING_TRC, DB_TABLE_MAILING_SDATE, DB_TABLE_MAILING_STIME, DB_TABLE_MAILING_INTER, DB_TABLE_MAILING_INTERIDX)
    */
   public static function get_tracing($k1, $k2, $k3) { 

      $k1_pro = htmlspecialchars($k1);
      $k2_pro = htmlspecialchars($k2);
      $k3_pro = htmlspecialchars($k3);

      $sql_select = "SELECT ".$GLOBALS['DB_TABLE_TRACING_STATUS']." ".
                    "FROM ".$GLOBALS['DB_TABLE_TRACING']." ".
                    "WHERE ".$GLOBALS['DB_TABLE_TRACING_K1']." = '".$k1_pro."' ".
                    "AND ".$GLOBALS['DB_TABLE_TRACING_K2']." = '".$k2_pro."' ".
                    "AND ".$GLOBALS['DB_TABLE_TRACING_K3']." = '".$k3_pro."';";
            
      $mydbsel = new db_select();
      $mydbsel->exec($sql_select);
      
      if($mydbsel->numrow() == 1)
         return $mydbsel->fetchconvresults();
      else if($mydbsel->numrow() == 0)
         return 0;
      
      return -1;
   }
   
   /** Get all the mail to send from DB_TABLE_MAILING
    *  @return array(DB_TABLE_MAILING_SUB, DB_TABLE_MAILING_FROM, DB_TABLE_MAILING_REP, DB_TABLE_MAILING_HEAD, DB_TABLE_MAILING_TO, DB_TABLE_MAILING_MSG, DB_TABLE_MAILING_TRC, DB_TABLE_MAILING_SDATE, DB_TABLE_MAILING_STIME, DB_TABLE_MAILING_INTER, DB_TABLE_MAILING_INTERIDX)
    */
   public static function get_mail() { 

      $sql_select = "SELECT id, ".
                    "".$GLOBALS['DB_TABLE_MAILING_SUB'].", ".
                    "".$GLOBALS['DB_TABLE_MAILING_FROM'].", ".
                    "".$GLOBALS['DB_TABLE_MAILING_REP'].", ".
                    "".$GLOBALS['DB_TABLE_MAILING_HEAD'].", ".
                    "".$GLOBALS['DB_TABLE_MAILING_TO'].", ".
                    "".$GLOBALS['DB_TABLE_MAILING_MSG'].", ".
                    "".$GLOBALS['DB_TABLE_MAILING_TRC'].", ".
                    "".$GLOBALS['DB_TABLE_MAILING_SDATE'].", ".
                    "".$GLOBALS['DB_TABLE_MAILING_STIME'].", ".
                    "".$GLOBALS['DB_TABLE_MAILING_INTER'].", ".
                    "".$GLOBALS['DB_TABLE_MAILING_INTERIDX']." ".
                    "FROM ".$GLOBALS['DB_TABLE_MAILING']." ".
                    "WHERE ".$GLOBALS['DB_TABLE_MAILING_STAT']." = 0;";
            
      $mydbsel = new db_select();
      $mydbsel->exec($sql_select);
      
      if($mydbsel->numrow() == 1)
         return $mydbsel->fetchconvresults();
      else if($mydbsel->numrow() == 0)
         return 0;
      
      return -1;
   }
   
   /** update the mailing in DB_TABLE_MAILING
    *  @param id int
    *  @param last_send_date string
    *  @param next_send_timing int
    *  @param interval_index int
    *  @param status int (conditionnal) 0 in case of no use
    */
   public static function upd_mail($id, $last_send_date, $next_send_timing, $interval_index, $status) { 
   
      $id_pro = htmlspecialchars($id);
      $last_send_date_pro = htmlspecialchars($last_send_date);
      $next_send_timing_pro = htmlspecialchars($next_send_timing);
      $interval_index_pro = htmlspecialchars($interval_index);
      $status_pro = "";
      
      if($status != 0) {
         $status_pro = ", ".$GLOBALS['DB_TABLE_MAILING_STAT']." = ".htmlspecialchars($status);
      }
      
      $update_sql = "UPDATE ".$GLOBALS['DB_TABLE_MAILING']." SET ".
                    $GLOBALS['DB_TABLE_MAILING_SDATE']." = '".$last_send_date."', ".
                    $GLOBALS['DB_TABLE_MAILING_STIME']." = ".$next_send_timing.", ".
                    $GLOBALS['DB_TABLE_MAILING_INTERIDX']." = ".$interval_index.$status_pro."".
                    " WHERE id = ".$id_pro.";";

      $mydbupd = new db_update();
      $mydbupd->exec($update_sql);
   }
   
   
   
   /** Insert tracing in the DB_TABLE_TRACING
    *  @param k1 string
    *  @param k2 string
    *  @param k3 string
    *  @param status int
    *  @param send_date string
    */
   public static function add_tracing($k1, $k2, $k3, $status, $send_date) { 

      $k1_pro = htmlspecialchars($k1);
      $k2_pro = htmlspecialchars($k2);
      $k3_pro = htmlspecialchars($k3);
      $status_pro = htmlspecialchars($status);
      $send_date_pro = htmlspecialchars($send_date);
   
      $sql_insert = "INSERT INTO ".$GLOBALS['DB_TABLE_TRACING']."".
                    "(".$GLOBALS['DB_TABLE_TRACING_K1'].", ".
                        $GLOBALS['DB_TABLE_TRACING_K2'].", ".
                        $GLOBALS['DB_TABLE_TRACING_K3'].", ".
                        $GLOBALS['DB_TABLE_TRACING_STATUS'].", ".
                        $GLOBALS['DB_TABLE_TRACING_SDATE'].") ".
                   "VALUES ('".$k1_pro."', ".
                           "'".$k2_pro."', ".
                           "'".$k3_pro."', ".
                           "".$status_pro.", ".
                           "'".$send_date_pro."');";
 
      $mydbins = new db_insert();
      $mydbins->exec($sql_insert);
   }
   
   /** Add mail in the DB_TABLE_MAILING
    *  @param subject string
    */
   public static function add_mail($subject, $mail_from, $reply_to, $header, $mail_to, $message, $tracing, $last_send_date, $next_send_timing, $mail_interval, $status, $interval_index) { 
   
      $user_mail_pro = htmlspecialchars($subject);
      $mail_from_pro = htmlspecialchars($mail_from);
      $reply_to_pro = htmlspecialchars($reply_to);
      $header_pro = htmlspecialchars($header);
      $mail_to_pro = htmlspecialchars($mail_to);
      $message_pro = htmlspecialchars($message);
      $tracing_pro = htmlspecialchars($tracing);
      $last_send_date_pro = htmlspecialchars($last_send_date);
      $next_send_timing_pro = htmlspecialchars($next_send_timing);
      $mail_interval_pro = htmlspecialchars($mail_interval);
      $status_pro = htmlspecialchars($status);
      $interval_index_pro = htmlspecialchars($interval_index);
   
      $sql_insert = "INSERT INTO ".$GLOBALS['DB_TABLE_MAILING']."".
                                 "(".$GLOBALS['DB_TABLE_MAILING_SUB'].", ".
                                     $GLOBALS['DB_TABLE_MAILING_FROM'].", ".
                                     $GLOBALS['DB_TABLE_MAILING_REP'].", ".
                                     $GLOBALS['DB_TABLE_MAILING_HEAD'].", ".
                                     $GLOBALS['DB_TABLE_MAILING_TO'].", ".
                                     $GLOBALS['DB_TABLE_MAILING_MSG'].", ".
                                     $GLOBALS['DB_TABLE_MAILING_TRC'].", ".
                                     $GLOBALS['DB_TABLE_MAILING_SDATE'].", ".
                                     $GLOBALS['DB_TABLE_MAILING_STIME'].", ".
                                     $GLOBALS['DB_TABLE_MAILING_INTER'].", ".
                                     $GLOBALS['DB_TABLE_MAILING_STAT'].", ".
                                     $GLOBALS['DB_TABLE_MAILING_INTERIDX'].") ".
                     "VALUES('".$user_mail_pro."', ".
                            "'".$mail_from_pro."', ".
                            "'".$reply_to_pro."', ".
                            "'".$header_pro."', ".
                            "'".$mail_to_pro."', ".
                            "'".$message_pro."', ".
                            "".$tracing_pro.", ".
                            "'".$last_send_date_pro."', ".
                            "".$next_send_timing_pro.", ".
                            "'".$mail_interval_pro."', ".
                            "".$status_pro.", ".
                            "".$interval_index_pro.");";
                  
      $mydbins = new db_insert();
      $mydbins->exec($sql_insert);
   }
   
   /** Get the id, user_mail, user_password, activated, email_code from the DB_TABLE_US
    *  @param user_mail string
    *  @return -1 failure / 0 no user / array(DB_TABLE_US_MAIL, DB_TABLE_US_PASS, DB_TABLE_US_ACTIVATED, DB_TABLE_US_EMAIL_CODE)
    */
   public static function get_login_info($user_mail) { 
   
      $user_mail_pro = htmlspecialchars($user_mail);
   
      $sql_select = "SELECT id, ".$GLOBALS['DB_TABLE_US_MAIL'].", ".
                                  $GLOBALS['DB_TABLE_US_PASS'].", ".
                                  $GLOBALS['DB_TABLE_US_ACTIVATED'].", ".
                                  $GLOBALS['DB_TABLE_US_EMAIL_CODE']." ".
                    "FROM ".$GLOBALS['DB_TABLE_US']." ".
                    "WHERE ".$GLOBALS['DB_TABLE_US_MAIL']." = '".$user_mail_pro."';";
            
      $mydbsel = new db_select();
      $mydbsel->exec($sql_select);
      
      if($mydbsel->numrow() == 1)
         return $mydbsel->fetchconvresults();
      else if($mydbsel->numrow() == 0)
         return 0;
      
      return -1;
   }
   
   /** update the password in DB_TABLE_US
    *  @param user_mail string
    *  @param user_password string
    */
   public static function upd_password($user_mail, $user_password) { 
   
      $user_mail_pro = htmlspecialchars($user_mail);
      $user_password_pro = htmlspecialchars($user_password);
   
      $update_sql = "UPDATE ".$GLOBALS['DB_TABLE_US']." ".
                    "SET ".$GLOBALS['DB_TABLE_US_PASS']." = '".$user_password_pro."', ".
                    "".$GLOBALS['DB_TABLE_US_ACTIVATED']." = 2 ".
                    "WHERE ".$GLOBALS['DB_TABLE_US_MAIL']." = '".$user_mail_pro."';";
               
      $mydbupd = new db_update();
      $mydbupd->exec($update_sql);
   }
   
   /** Get the id from the DB_TABLE_US
    *  @param user_mail string
    *  @return -1 failure / id
    */
   public static function get_id($user_mail) { 
   
      $user_mail_pro = htmlspecialchars($user_mail);
   
      $sql_select = "SELECT id ".
                    "FROM ".$GLOBALS['DB_TABLE_US']." ".
                    "WHERE ".$GLOBALS['DB_TABLE_US_MAIL']." = '".$user_mail_pro."' ".
                    "AND ".$GLOBALS['DB_TABLE_US_ACTIVATED']." > 1;";
      
      $mydbsel = new db_select();
      $mydbsel->exec($sql_select);
      
      if($mydbsel->numrow() == 1)
         return $mydbsel->fetchconvresults()[0];
      
      return -1;
   }
   
   /** Get the user power from the DB_TABLE_US
    *  @param user_id int
    *  @param user_mail string
    *  @param user_pass string
    *  @return -1 failure / user_power
    */
   public static function get_power($user_id, $user_mail, $user_pass) { 
   
      $user_id_pro = htmlspecialchars($user_id);
      $user_mail_pro = htmlspecialchars($user_mail);
      $user_pass_pro = htmlspecialchars($user_pass);
   
      $sql_select = "SELECT ".$GLOBALS['DB_TABLE_US_POWER']." ".
                    "FROM ".$GLOBALS['DB_TABLE_US']." ".
                    "WHERE id = '".$user_id_pro."' ".
                    "AND ".$GLOBALS['DB_TABLE_US_MAIL']." = '".$user_mail_pro."' ".
                    "AND ".$GLOBALS['DB_TABLE_US_PASS']." = '".$user_pass_pro."' ".
                    "AND ".$GLOBALS['DB_TABLE_US_ACTIVATED']." = 1;";
      
      $mydbsel = new db_select();
      $mydbsel->exec($sql_select);
      
      if($mydbsel->numrow() == 1)
         return $mydbsel->fetchconvresults()[0];
      
      return -1;
   }
   
   /** Get the sign code and activation index from the DB_TABLE_US
    *  @param user_mail string
    *  @return -1 failure / 0 if no user / array(DB_TABLE_US_SIGN_CODE, DB_TABLE_US_ACTIVATED)
    */
   public static function get_signup_info($user_mail) { 
      
      $user_mail_pro = htmlspecialchars($user_mail);
      
      $sql_select = "SELECT ".$GLOBALS['DB_TABLE_US_SIGN_CODE'].", ".
                              $GLOBALS['DB_TABLE_US_ACTIVATED']." ".
                    "FROM ".$GLOBALS['DB_TABLE_US']." ".
                    "WHERE ".$GLOBALS['DB_TABLE_US_MAIL']." = '".$user_mail_pro."';";
      
      $mydbsel = new db_select();
      $mydbsel->exec($sql_select);
      
      if($mydbsel->numrow() == 0)
         return 0;
      else if($mydbsel->numrow() > 0)
         return $mydbsel->fetchconvresults();
      
      return -1;
   }
   
   /** Get the id and activation index from the DB_TABLE_US
    *  @param user_mail string
    *  @param email_code string
    *  @return -1 failure / 0 if no user / array(ID, DB_TABLE_US_ACTIVATED)
    */
   public static function get_email_info($user_mail, $email_code) { 
      
      $user_mail_pro = htmlspecialchars($user_mail);
      $email_code_pro = htmlspecialchars($email_code);
      
      $sql_select = "SELECT id, ".$GLOBALS['DB_TABLE_US_ACTIVATED']." ".
                    "FROM ".$GLOBALS['DB_TABLE_US']." ".
                    "WHERE ".$GLOBALS['DB_TABLE_US_MAIL']." = '".$user_mail_pro."' ".
                    "AND ".$GLOBALS['DB_TABLE_US_SIGN_CODE']." = '".$email_code_pro."';";
      
      $mydbsel = new db_select();
      $mydbsel->exec($sql_select);
      
      if($mydbsel->numrow() == 0)
         return 0;
      else if($mydbsel->numrow() > 0)
         return $mydbsel->fetchconvresults();
      
      return -1;
   }
   
   /** Update the activation from the DB_TABLE_US
    *  @param user_id int
    */
   public static function upd_activation_info($user_id) { 
      
      $user_id_pro = htmlspecialchars($user_id);
      
      $update_sql = "UPDATE ".$GLOBALS['DB_TABLE_US']." ".
                        "SET ".$GLOBALS['DB_TABLE_US_ACTIVATED']." = 1 ".
                        "WHERE id = ".$user_id_pro.";";
      
      $mydbupd = new db_update();
      $mydbupd->exec($update_sql);
   }
   
   /** Insert new user in the DB_TABLE_US
    *  @param first_name string
    *  @param second_name string
    *  @param user_password v
    *  @param user_mail string
    *  @param user_mobile string
    *  @param user_country string
    *  @param user_birthday string
    *  @param user_gender string
    *  @param user_power int
    *  @param sign_code string
    *  @param email_code string
    *  @param activated int
    */
   public static function add_user($first_name, $second_name, $user_password, $user_mail, $user_mobile, $user_country, $user_birthday, $user_gender, $user_power, $sign_code, $email_code, $activated) { 
      
      $first_name_pro = htmlspecialchars($first_name);
      $second_name_pro = htmlspecialchars($second_name);
      $user_password_pro = htmlspecialchars($user_password);
      $user_mail_pro = htmlspecialchars($user_mail);
      $user_mobile_pro = htmlspecialchars($user_mobile);
      $user_country_pro = htmlspecialchars($user_country);
      $user_birthday_pro = htmlspecialchars($user_birthday);
      $user_gender_pro = htmlspecialchars($user_gender);
      $user_power_pro = htmlspecialchars($user_power);
      $sign_code_pro = htmlspecialchars($sign_code);
      $email_code_pro = htmlspecialchars($email_code);
      $activated_pro = htmlspecialchars($activated);
      
      $sql_insert = "INSERT INTO ".$GLOBALS['DB_TABLE_US']."".
                    "(".$GLOBALS['DB_TABLE_US_FIRST'].", ".
                        $GLOBALS['DB_TABLE_US_SECOND'].", ".
                        $GLOBALS['DB_TABLE_US_PASS'].", ".
                        $GLOBALS['DB_TABLE_US_MAIL'].", ".
                        $GLOBALS['DB_TABLE_US_MOBILE'].", ".
                        $GLOBALS['DB_TABLE_US_COUNTRY'].", ".
                        $GLOBALS['DB_TABLE_US_BIRTHDAY'].", ".
                        $GLOBALS['DB_TABLE_US_GENDER'].", ".
                        $GLOBALS['DB_TABLE_US_POWER'].", ".
                        $GLOBALS['DB_TABLE_US_SIGN_CODE'].", ".
                        $GLOBALS['DB_TABLE_US_EMAIL_CODE'].", ".
                        $GLOBALS['DB_TABLE_US_ACTIVATED'].") ".
                    "VALUES('".$first_name_pro."',
                            '".$second_name_pro."',
                            '".$user_password_pro."',
                            '".$user_mail_pro."',
                            '".$user_mobile_pro."',
                            '".$user_country_pro."',
                            '".$user_birthday_pro."',
                            '".$user_gender_pro."',
                            1,
                            '".$sign_code_pro."',
                            '".$email_code_pro."',
                            0);";
                            
      $mydbins = new db_insert();
      $mydbins->exec($sql_insert);
   }
   
}

?>
<?php 
/*
 * Copyright (c) 2019 ESWAR and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * Please contact ESWAR main developper : redtsubasa1 - RT1
 * or visit www.es-war.com if you need additional information or have any
 * questions.
 */

require_once('esw_system/esw_library/phpqrcode/qrlib.php');
require_once("esw_system/esw_core/sql_handler.php");

class qr_code {
    //build info must be as : $var1;$var2;$var3 and so on ...
    public static function gen_qr($build_info, $identificator) {
        $quality = QR_ECLEVEL_M;
        $deepness = 4;
        $builder = explode(";", $build_info);
        $sync = "";
        $extension = ".png";
        $conv_separator = "_";
        $path = "/home/cashzone/api-esw.cash-zone.fr/esw_product/qrcode/qrcodes/";
        $html_path = $GLOBALS['dns']."/esw_system/esw_library/qrcode/qrcodes/";
        
        //we'll save our QR in database
        $mydbins = new db_insert();
        $mydbins->db_obj->redocon("/home/cashzone/api-esw.cash-zone.fr/esw_config/info/coninf_estore.txt");
        
        //link all the needed info in order to build our QR code
        for($i = 0; $i < count($builder); $sync .= $builder[$i++]);
        
        //hash the info
        $sync = md5($sync);
        $qr_file_name = $sync.$conv_separator.$identificator.$conv_separator.$deepness.$extension;
        
        //generate the qr
        QRcode::png($sync, $path.$qr_file_name, $quality, $deepness);
        
        //save it
        $sql = "INSERT INTO esport_qrcode(identificator, filename, filepath) VALUES ('".$identificator."', '".$qr_file_name."', '".$path."');";
        $mydbins->exec($sql);
        
        return $html_path.$qr_file_name;
    }
    
    //get the code for the mail subject
    public static function gen_code($build_info) {
        
        $sync = "";
        $builder = explode(";", $build_info);
        for($i = 0; $i < count($builder); $sync .= $builder[$i++]);
        $sync = md5($sync);
        
        return strtoupper(substr($sync, 0, 3).substr($sync, strlen($sync) - 3, 3));
    }
}
 
?>
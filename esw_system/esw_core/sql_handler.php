<?php

/*
 * Copyright (c) 2019 ESWAR and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * Please contact ESWAR main developper
 * or visit www.es-war.com if you need additional information or have any
 * questions.
 */

define("SEL", "select");
define("ADD", "insert");
define("MOD", "update");
define("DEL", "delete");

//require_once('esw_system/esw_core/hash_db.php');

class NotFoundException extends Exception {}

class db_eswar extends mysqli {
   protected $db_link;
   protected $db_con;
   private $err_words = array();
   public $result;
   
   public function __construct() {
      parent::init();
      
      //if(md5_file("esw_system/esw_core/hash_db.php") == "dfe53d88eb7af4da53d96e8ae7a48a82") Binou::decode("esw_system/esw_core/crypto_task/database.txt");
      
      if(!parent::options(MYSQLI_INIT_COMMAND, 'SET AUTOCOMMIT = 0'))
         die('Setting MYSQLI_INIT_COMMAND failed');

      if(!parent::options(MYSQLI_OPT_CONNECT_TIMEOUT, 5))
         die('Setting MYSQLI_OPT_CONNECT_TIMEOUT failed');

      //if(!parent::real_connect(Binou::h(), Binou::u(), Binou::p(), Binou::d()))
      if(!parent::real_connect("127.0.0.1", "knucklesskills", "-#^^$?@@°°É♦xW+ÏrÍë2g%↓ÎÅ•1", "eswar"))
         die('Connect Error (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
      
      //$this->owndatabase(Binou::h(), Binou::u(), Binou::p(), Binou::d());
      $this->owndatabase("127.0.0.1", "knucklesskills", "-#^^$?@@°°É♦xW+ÏrÍë2g%↓ÎÅ•1", "eswar");
      
      if($this->checkcon() != true)
        throw new NotFoundException();
   }
   
   //create a both mysqli and connect LINK to a database
   private function owndatabase($host, $user, $pass, $db) {
      $this->ownconnect($host, $user, $pass, $db);
      $this->owndbcon($host, $user, $pass, $db);
   }
   
   public function redocon($path) {
      $this->esclose();
      Binou::decode($path);
      parent::init();
      
      if(!parent::options(MYSQLI_INIT_COMMAND, 'SET AUTOCOMMIT = 0'))
      die('Setting MYSQLI_INIT_COMMAND failed');

      if(!parent::options(MYSQLI_OPT_CONNECT_TIMEOUT, 5))
         die('Setting MYSQLI_OPT_CONNECT_TIMEOUT failed');

      if(!parent::real_connect(Binou::h(), Binou::u(), Binou::p(), Binou::d()))
         die('Connect Error (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
      
      $this->owndatabase("127.0.0.1", "knucklesskills", "-#^^$?@@°°É♦xW+ÏrÍë2g%↓ÎÅ•1", "eswar");
      $this->checkcon();
   }
   
   //link handle substitution
   private function ownconnect($host, $user, $pass, $db) {
      $this->db_link = mysqli_connect($host, $user, $pass, $db);
      
      if($this->db_link->connect_errno) {
         echo "Connexion LINK error : ".$this->db_link->connect_error."<br>";
         $this->db_link = NULL;
         return 0;
      }
   }
   
   //close a connexion (the mysqli since that's the one we use for our queries)
   public function esclose() {
      if($this->db_con)
         $this->db_con->close();
         
      unset($this->db_con);
   }
   
   public function __sleep() {
      return array('host', 'user', 'pass', 'db');
   }
   
   public function __wakeup() {
      $this->ownconnect();
   }
   
   //persistent connexion to database
   private function owndbcon($host, $user, $pass, $db) {
      $this->db_con = new mysqli($host, $user, $pass, $db);
      
      if($this->db_con->connect_errno) {
         echo "Connexion PLINK error : ".$this->db_con->connect_error."<br>";
         $this->db_con = NULL;
         return 0;
      }
      return 1;
   }
   
   //private implementation for query handling
   public function esquery($sql) {

      if($this->checkcon())
      {
         //check for the semi-colon ending
         if($sql[strlen($sql) - 1] != ';')
         {
            echo "<br>Semi-colon ending not found in your SQL query : ".$sql."<br>";
            return false;
         }
         else {
            $result = $this->db_con->query($sql);
            
            //check in case of mysqli errors !
            if(!$result) {
               echo "<br>MYSQLI error : ".$this->db_con->error."<br>";
               echo "in : ".$sql;
               return false;
            } else {   
               return $result;
            }
         }
      }
   }
   
   //check connexion status
   public function checkcon() {
      $flag_dead = false;
       
      if($this->db_link == NULL) {
         echo "DB link is NULL.<br>";
         $flag_dead = true;
      }
      
      if($this->db_con == NULL) {
         echo "DB Plink is NULL.<br>";
         $flag_dead = true;
      }
      
      if($this->db_link->connect_errno) {
         echo "Connexion LINK error : ".$this->db_link->connect_error."<br>";
         $flag_dead = true;
      }
      
      if($this->db_con->connect_errno) {
         echo "Connexion PLINK error : ".$this->db_con->connect_error."<br>";
         $flag_dead = true;
      }
      
      if(!$this->db_link->ping()) {
         echo "Connexion LINK is dead : ".$this->db_link->error."<br>";
         $flag_dead = true;
      }
      
      return !$flag_dead;
   }
}

//our main handler for all type of db query
abstract class db_handler {
    //db_con handler
    public $db_obj;
    
    //internal iterator for $sqlplan_arr context $sqlplan_arr[$internal_itt][x,y,z]
    //where w : 0/1 success, x : query, y : result, z : numrow
    protected $internal_itt;
    protected $sqlplan_arr = array();
    
    //restricted MySQL keyword by types
    private $restricted_data_definition_statements = array("ALTER DATABASE", "ALTER EVENT", "ALTER FUNCTION", "ALTER INSTANCE", "ALTER LOGFILE GROUP", "ALTER PROCEDURE", "ALTER SERVER", "ALTER TABLE", "ALTER TABLESPACE", "ALTER VIEW", "CREATE DATABASE", "CREATE EVENT", "CREATE FUNCTION", "CREATE INDEX", "CREATE LOGFILE GROUP", "CREATE PROCEDURE", "CREATE FUNCTION", "CREATE SERVER", "CREATE SPATIAL REFERENCE SYSTEM", "CREATE TABLE", "CREATE TABLESPACE", "CREATE TRIGGER", "CREATE VIEW", "DROP DATABASE", "DROP EVENT", "DROP FUNCTION", "DROP INDEX", "DROP LOGFILE GROUP", "DROP PROCEDURE", "DROP FUNCTION", "DROP SERVER", "DROP SPATIAL REFERENCE SYSTEM", "DROP TABLE", "DROP TABLESPACE", "DROP TRIGGER", "DROP VIEW", "RENAME TABLE", "TRUNCATE TABLE");
    private $restricted_user_modify_implicit_statements = array("ALTER USER", "CREATE USER", "DROP USER", "GRANT", "RENAME USER", "REVOKE", "SET PASSWORD");
    private $restricted_locking_statements = array("UNLOCK TABLES", "LOCK TABLES", "FLUSH TABLES", "READ LOCK", "START");
    private $rectricted_data_load_statements = array("LOAD DATA");
    private $restricted_admin_statements = array("ANALYZE TABLE", "CACHE INDEX", "CHECK TABLE", "FLUSH", "LOAD INDEX INTO CACHE", "OPTIMIZE TABLE", "REPAIR TABLE");
    private $restricted_control_statements = array("START SLAVE", "STOP SLAVE", "RESET SLAVE", "CHANGE MASTER TO");
    
    //handler assignment constructor
    public function __construct($type) {
        if(strcmp($type, SEL) !== 0 && strcmp($type, ADD) !== 0 && strcmp($type, MOD) !== 0 && strcmp($type, DEL) !== 0)
            throw new NotFoundException();

        $this->db_obj = new db_eswar;

        if($this->db_obj->checkcon() != true)
            throw new NotFoundException();

        $this->internal_itt = 0;
        $this->sqlplan_arr[$this->internal_itt] = array();
    }
    
    //handler destructor
    public function __destruct() {
        $this->db_obj->esclose();
    }
    
    //check whenever a restricted usage if found for a based query plan
    public function preg_restricted_usage($query, $restricted_arr) {
        $sqlerr = "";
        $errflag = 0;
        foreach($restricted_arr as &$value) {
            if(preg_match("/".$value."/i" , $query)) {
                $sqlerr .= ($errflag) ? "/<b>".$value."</b>" : "<b>".$value."</b>";
                $errflag = true;
            }
        }
        if($errflag) {
            echo "Restricted data definition statements : ".$sqlerr." in SQL : \"".$query."\"<br>";
            return 0;
        } else
            return 1;
    }
    
    //control and exec post prone error the based query plan
    public function exec($query) {
        if($this->preg_restricted_usage($query, $this->restricted_data_definition_statements) &&
           $this->preg_restricted_usage($query, $this->restricted_user_modify_implicit_statements) &&
           $this->preg_restricted_usage($query, $this->restricted_locking_statements) &&
           $this->preg_restricted_usage($query, $this->rectricted_data_load_statements) &&
           $this->preg_restricted_usage($query, $this->restricted_admin_statements) &&
           $this->preg_restricted_usage($query, $this->restricted_control_statements)) 
        {
            ++$this->internal_itt;
            $this->sqlplan_arr[$this->internal_itt] = array();
            
            //save the result since the esquery execute the query (do not call it 2 time)
            $tmp_query_flag = $this->db_obj->esquery($query);
           
            //do if only the mysqli query is valid
            if($tmp_query_flag)
            {
               //register success
               array_push($this->sqlplan_arr[$this->internal_itt], 1);
               
               //save the query for history
               array_push($this->sqlplan_arr[$this->internal_itt], $query);
               
               //save the result for history
               array_push($this->sqlplan_arr[$this->internal_itt], $tmp_query_flag);

               //save the number of row, only for SELECT queries
               if(strpos($query, "SELECT") !== false) {
                  array_push($this->sqlplan_arr[$this->internal_itt], $tmp_query_flag->num_rows);
               }
            } 
            //in case of mysqli error also register the error
            else {
               //register mysqli failure
               array_push($this->sqlplan_arr[$this->internal_itt], 0);
            }
        }
    }
    
    public function numrow() {
      if($this->sqlplan_arr[$this->internal_itt][0])
         return $this->sqlplan_arr[$this->internal_itt][3];
    }
    
    //in order to automate the fetching result
    public function fetchasnum() {
      if($this->sqlplan_arr[$this->internal_itt][0])
         $this->sqlplan_arr[$this->internal_itt][2]->fetch_array(MYSQLI_NUM);
    }
    
    //default child ownership definition, based on query type
    abstract function fetchresults();
}

//select object handler
class db_insert extends db_handler {
    //restricted data manipulation statements by type of handler
    private $restricted_data_manipulation_statements = array("CALL", "DELETE", /*"DO",*/ "HANDLER", "IMPORT TABLE", /*"INSERT",*/ "LOAD DATA", "LOAD XML", "REPLACE", "SELECT", "Subquery", "UPDATE", "WITH");
    
    function __construct() {
        parent::__construct(ADD);
    }
    
    function __destruct() {
        parent::__destruct();
    }
    
    public function exec($query) {
        //check in order to find "INSERT" statement
        if(strpos($query, "INSERT") === false)
            echo "<br>'INSERT' statement not found in SQL query : ".$query."<br>";
        else if($this->preg_restricted_usage($query, $this->restricted_data_manipulation_statements))
            parent::exec($query);
    }

    //show the result (can be expanded as template)
    public function fetchresults() {
        return $this->db_obj->affected_rows;
    }
}

//select object handler
class db_select extends db_handler {
    //restricted data manipulation statements by type of handler
    private $restricted_data_manipulation_statements = array("CALL", "DELETE", /*"DO",*/ "HANDLER", "IMPORT TABLE", "INSERT", "LOAD DATA", "LOAD XML", "REPLACE", /*"SELECT", */ "Subquery", "UPDATE", "WITH");
    
    function __construct() {
        parent::__construct(SEL);
    }
    
    function __destruct() {
        parent::__destruct();
    }
    
    public function exec($query) {
        //check in order to find "SELECT" statement
        if(strpos($query, "SELECT") === false)
            echo "<br>'SELECT' statement not found in SQL query : ".$query."<br>";
        else if($this->preg_restricted_usage($query, $this->restricted_data_manipulation_statements))
            parent::exec($query);
    }

    //show the result (can be expanded as template)
    public function fetchresults() {
      if($this->sqlplan_arr[$this->internal_itt][0])
         return $this->sqlplan_arr[$this->internal_itt][2];
    }
    
    //0 as MYSQLI_NUM
    //1 as MYSQLI_ASSOC
    //2 as MYSQLI_BOTH
    //default as MYSQLI_BOTH
    public function fetchconvresults() {
      if($this->sqlplan_arr[$this->internal_itt][0])
         return $this->sqlplan_arr[$this->internal_itt][2]->fetch_array(MYSQLI_BOTH);
    }
}

//select object handler
class db_update extends db_handler {
    //restricted data manipulation statements by type of handler
    private $restricted_data_manipulation_statements = array("CALL", "DELETE", /*"DO",*/ "HANDLER", "IMPORT TABLE", "INSERT", "LOAD DATA", "LOAD XML", "REPLACE", "SELECT", "Subquery", /*"UPDATE",*/ "WITH");
    
    function __construct() {
        parent::__construct(MOD);
    }
    
    function __destruct() {
        parent::__destruct();
    }
    
    public function exec($query) {
        //check in order to find "UPDATE" statement
        if(strpos($query, "UPDATE") === false)
            echo "<br>'UPDATE' statement not found in SQL query : ".$query."<br>";
        else if($this->preg_restricted_usage($query, $this->restricted_data_manipulation_statements))
            parent::exec($query);
    }

    //show the result (can be expanded as template)
    public function fetchresults() {
        return $this->db_obj->affected_rows;
    }
}

//delete object handler
class db_delete extends db_handler {
    //restricted data manipulation statements by type of handler
    private $restricted_data_manipulation_statements = array("CALL", /*"DELETE"*/ /*"DO",*/ "HANDLER", "IMPORT TABLE", "INSERT", "LOAD DATA", "LOAD XML", "REPLACE", "SELECT", "Subquery", "UPDATE", "WITH");
    
    function __construct() {
        parent::__construct(DEL);
    }
    
    function __destruct() {
        parent::__destruct();
    }
    
    public function exec($query) {
        //check in order to find "DELETE" statement
        if(strpos($query, "DELETE") === false)
            echo "<br>'DELETE' statement not found in SQL query : ".$query."<br>";
        else if($this->preg_restricted_usage($query, $this->restricted_data_manipulation_statements))
            parent::exec($query);
    }

    //show the result (can be expanded as template)
    public function fetchresults() {
        return $this->db_obj->affected_rows;
    }
}

?>
<?php

/*
 * Copyright (c) 2019 ESWAR and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * Please contact ESWAR main developper : redtsubasa1 - RT1
 * or visit www.es-war.com if you need additional information or have any
 * questions.
 */

class pwd_mgt {
   private $meta_array;
   private $hash;
   public $code;
   
   public function __construct() {
      /**
       * idx : 0 = usr_first_name
       * idx : 1 = usr_second_name
       * idx : 2 = usr_email
       * idx : 3 = usr_mobile
       * idx : 4 = usr_country
       * idx : 5 = usr_birthday
       * idx : 6 = usr_gender
       * idx : 7 = time
       * idx : 8 = ip
       * idx : 9 = tmp pw
       */
      $this->meta_array = array();
   }
    
   //build the user meta array
   public function signup_pw($usr_first_name, $usr_second_name, $usr_email, $usr_mobile, $usr_country, $usr_birthday, $usr_gender) {
      
      array_push($this->meta_array, $usr_first_name, $usr_second_name, $usr_email, $usr_mobile, $usr_country, $usr_birthday, $usr_gender);
      
      $this->gen_pw();
      
      return $this->code;
   }
   
   public function email_pw($code) {
   
      $this->code = md5(base64_encode($code));
      $this->code = substr($this->code, mt_rand(0, strlen($this->code) - 12), 12);
      
      return strtoupper($this->code);
   }
   
   public function email_sha3($code) {
      return hash('sha3-512', htmlspecialchars($code));
   }
   
   //first password is always in base64
   private function gen_pw() {
   
      //Unix timestamp
      array_push($this->meta_array, str_replace(array(' ', '.'), "", microtime()));
      
      //REMOTE_ADDR will do the job we're not trying to check VPN's
      array_push($this->meta_array, getenv('REMOTE_ADDR'));
      
      foreach($this->meta_array as &$value) {
		 
		 
         $this->hash .= md5($value);
      }
      
      //hash to b64
      $this->hash = strtoupper(base64_encode($this->hash));
      
      //build the signup pwd code as follow : xxxx-xxxx-xxxx-xxxx
      for($i = 0; $i < 4; ++$i) {
         $this->code .= substr($this->hash, mt_rand(0, strlen($this->hash) - 4), 4)."-";
      }
      
      $this->code = $this->hash;
   }
}

?>
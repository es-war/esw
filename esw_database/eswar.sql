-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 28 jan. 2021 à 18:08
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP : 7.2.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `eswar`
--

-- --------------------------------------------------------

--
-- Structure de la table `esport_mail`
--

CREATE TABLE `esport_mail` (
  `id` int(11) NOT NULL,
  `subject` varchar(2500) NOT NULL,
  `mail_from` varchar(250) NOT NULL,
  `reply_to` varchar(250) NOT NULL,
  `header` varchar(250) NOT NULL,
  `mail_to` varchar(250) NOT NULL,
  `message` varchar(250) NOT NULL,
  `tracing` int(4) NOT NULL,
  `last_send_date` varchar(250) NOT NULL,
  `next_send_timing` int(16) NOT NULL,
  `mail_interval` varchar(250) NOT NULL,
  `status` int(8) NOT NULL,
  `interval_index` int(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `esport_mail`
--

INSERT INTO `esport_mail` (`id`, `subject`, `mail_from`, `reply_to`, `header`, `mail_to`, `message`, `tracing`, `last_send_date`, `next_send_timing`, `mail_interval`, `status`, `interval_index`) VALUES
(50, '🎫 Welcome in ESW : Account activation', 'From: ESW - Activation   <ticketing@esw.com>\r\n', 'Reply-To: support@esw.com\r\n', 'From: ESW - Activation   <ticketing@esw.com>\r\nReply-To: support@esw.com\r\nMIME-Version: 1.0\r\nContent-type: text/html; charset=utf-8\r\n', 'leonardkarin@gmail.com', '<a href=\'http://localhost/xampp/index.php?action=3&ack=2&pk1=bGVvbmFyZGthcmluQGdtYWlsLmNvbQ==&pk2=MTVM-MZOD-LJZM-Y5ND\'>ACTIVATE YOUR ACCOUNT</a><br><br>', 1, '2021-01-27 23:47:53', -1, '0', 1, -1);

-- --------------------------------------------------------

--
-- Structure de la table `esport_mail_tracing`
--

CREATE TABLE `esport_mail_tracing` (
  `id` int(11) NOT NULL,
  `k1` varchar(255) NOT NULL,
  `k2` varchar(255) NOT NULL,
  `k3` varchar(255) NOT NULL,
  `status` int(4) NOT NULL,
  `send_date` varchar(255) NOT NULL,
  `open_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `esport_mail_tracing`
--

INSERT INTO `esport_mail_tracing` (`id`, `k1`, `k2`, `k3`, `status`, `send_date`, `open_date`) VALUES
(47, 'f251253b86f096eea67562db026a65dc', '8af5fedefa6f96fd7416ac01603e9282', 'd95d22eae73f5ff5ee9db58b97824acb', 1, '2021-01-27 23:47:53', '2021-01-27 23:49:56');

-- --------------------------------------------------------

--
-- Structure de la table `esport_user`
--

CREATE TABLE `esport_user` (
  `id` int(4) NOT NULL,
  `first_name` varchar(128) NOT NULL,
  `second_name` varchar(128) NOT NULL,
  `user_nickname` varchar(64) NOT NULL,
  `user_password` varchar(1024) NOT NULL,
  `user_power` int(1) NOT NULL,
  `user_email` varchar(128) NOT NULL,
  `user_mobile` varchar(20) NOT NULL,
  `user_country` varchar(64) NOT NULL,
  `user_birthday` varchar(10) NOT NULL,
  `user_gender` varchar(20) NOT NULL,
  `sign_up_code` varchar(128) NOT NULL,
  `email_code` varchar(12) NOT NULL,
  `activated` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `esport_user`
--

INSERT INTO `esport_user` (`id`, `first_name`, `second_name`, `user_nickname`, `user_password`, `user_power`, `user_email`, `user_mobile`, `user_country`, `user_birthday`, `user_gender`, `sign_up_code`, `email_code`, `activated`) VALUES
(135, 'Pierre', 'Karin', '', '', 1, 'leonardkarin@gmail.com', '777777', 'France', '11/11/1111', 'Male', 'MTVM-MZOD-LJZM-Y5ND', 'FAD70350CB36', 0);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `esport_mail`
--
ALTER TABLE `esport_mail`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `esport_mail_tracing`
--
ALTER TABLE `esport_mail_tracing`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `esport_user`
--
ALTER TABLE `esport_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `esport_mail`
--
ALTER TABLE `esport_mail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT pour la table `esport_mail_tracing`
--
ALTER TABLE `esport_mail_tracing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT pour la table `esport_user`
--
ALTER TABLE `esport_user`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

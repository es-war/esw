<?php

/*
 * Copyright (c) 2019 ESWAR and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * Please contact ESWAR main developper
 * or visit www.es-war.com if you need additional information or have any
 * questions.
 */
 
include('Crypt/RSA.php');

$privatekey = "EKIPEKIP";//file_get_contents('private.key');

$rsa = new Crypt_RSA();
$rsa->loadKey($privatekey); //public

$plaintext = new Math_BigInteger('aaaaaa');

echo $rsa->_exponentiate($plaintext)->toBytes();

$rsa->loadKey($privatekey); //private
echo $rsa->decrypt($ciphertext);

/* 
class esw_genesis {
	protected $hash_on_player;
	
	public function __construct($wallet_uid, ) {
		
	}
}
*/

?>